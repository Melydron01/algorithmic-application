#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include "Configuration.h"

template <typename T>
class Assignment {
public:

    virtual ~Assignment() {

    }

//    virtual Configuration<T> * assign(CentroidArray<T> * centroids, Array<T> * items, Distance<T > * calculator, bool findsecondbest = false) = 0;
    virtual Configuration<T> * assign(CentroidArray<T> * centroids, Array_<T> * items, DistanceMatrix<TrackD * > * matrix, bool findsecondbest = false) = 0;

private:

};

#endif /* ASSIGNMENT_H */

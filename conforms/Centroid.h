#ifndef CENTROID_H
#define CENTROID_H

#include "Track.h"
#include "CentroidItemInfo.h"

using namespace std;

template<typename T>
class Centroid {
public:
    //    Centroid(){
    //        cout << "*********************************" << endl;
    //    }
    //    ~Centroid(){
    //        cout << "---------------------------------" << endl;
    //    }
    T center;
    int index;
    Array_<T> items;
    Array_< ItemCentroidInfo<T> * > item_infos;

    Centroid(T item) : center(item) {
    }

    Centroid(T item, int offset) : center(item), index(offset) {
//        cout << "New Centroid: " << index << endl;
    }

};


#endif /* CENTROIDS_H */

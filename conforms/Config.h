#ifndef CONFIG_H
#define CONFIG_H

//#define MIN_DELTA (0.000000000005)
//#define MAX_DELTA (0.00000000001)

#define MIN_DELTA (0.00000000005)
#define MAX_DELTA (0.0000000001)

#define DEFAULT_K (2)
#define DEFAULT_L (3)
#define STATS_EXECS (10)

#define DEFAULT_ERROR (0.0000000000001)
#define DEFAULT_KVEC (3)
#define DEFAULT_M_FOR_VTW ((1L << 32) - 5)
#define DEFAULT_W (4)

#endif /* CONFIG_H */

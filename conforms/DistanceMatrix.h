#ifndef DISTANCEMATRIX_H
#define DISTANCEMATRIX_H

#include "Distance.h"
#include <fstream>
#include <cstring>

template <typename T>
class DistanceMatrix {
public:
    int N;
    double ** distances;

    DistanceMatrix(int N) : N(N) {
        distances = new double*[N];
        for (int i = 0; i < N; i++) {
            distances[i] = new double[N];
        }
    }

    ~DistanceMatrix() {
        for (int i = 0; i < N; i++) {
            delete [] distances[i];
        }
        delete distances;
    }

    void calcFill(Array_< T > * set, Distance<T> * calculator, const char* fileName) {
        //        cout << "Filling with fileName: " << fileName << endl;
        ofstream out(fileName);
        streambuf *coutbuf = cout.rdbuf(); //save old buf
        cout.rdbuf(out.rdbuf()); //redirect cout to <output file>

        clock_t begin = clock();

        for (int i = 0; i < N; i++) {
            //            cout << "i: " << i << endl;
            if (i%50 == 0){
                printf("i: %d\n", i);
            }
            for (int j = 0; j <= i; j++) {
                if (i != j) {
                    distances[i][j] = calculator->calculate((*set)[i], (*set)[j]);
                } else {
                    //                    distances[i][j] = calculator->calculate((*set)[i], (*set)[j]);
                    distances[i][j] = 0;
                }
                cout << distances[i][j] << "\t[" << i << "," << j << "]" << endl;
            }
        }
        clock_t end = clock();

        double duration = ((double) (end - begin) / CLOCKS_PER_SEC);
        cout.rdbuf(coutbuf);
        cout << "Fill time: " << duration << endl;
    }

    void readFill(Array_< T > * set, Distance<T> * calculator, const char* fileName) {
        FILE * fp = fopen(fileName, "r");
        char buf[100000];
        char * pch;
        clock_t begin = clock();


        for (int i = 0; i < N; i++) {
            //            cout << "i: " << i << endl;

            for (int j = 0; j <= i; j++) {
                fgets(buf, sizeof buf, fp);
                pch = strtok(buf, "\t");
                if (i != j) {
                    distances[i][j] = strtold(pch, NULL);
                    //                    distances[i][j] = calculator->calculate((*set)[i], (*set)[j]);
                } else {
                    //                    distances[i][j] = calculator->calculate((*set)[i], (*set)[j]);
                    distances[i][j] = 0;
                }
                //                cout << distances[i][j] << "\t[" << i << "," << j << "]" << endl;
            }
        }

        fclose(fp);
        clock_t end = clock();
        double duration = ((double) (end - begin) / CLOCKS_PER_SEC);
        cout << "Fill time: " << duration << endl;


    }

    double getDistance(int i, int j) {
        if (i < j) {
            return distances[j][i];
        } else {
            return distances[i][j];
        }
    }
};

#endif /* DISTANCEMATRIX_H */


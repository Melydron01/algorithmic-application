#ifndef EUCLIDEANDISTANCE_H
#define EUCLIDEANDISTANCE_H

#include <cmath>

#include "Point.h"
#include "PointDistance.h"

template <typename T>
class EuclideanDistance : public PointDistance<T> {
public:

    virtual ~EuclideanDistance() {

    }

    double calculate(Point<T> * p1, Point<T> * p2) {
        size_t dims = max(p1->getD(), p2->getD());
        double dist = 0;
        for (size_t i = 0; i < dims; i++) {
            double x = 0;
            double y = 0;
            if (p2->size() > i) {
                x = (*p1)[i];
            }
            if (p1->size() > i) {
                y = (*p2)[i];
            }
            double temp = pow(x - y, 2);
            dist = dist + temp;
        }
        dist = sqrt(dist);
        return dist;
    }

    double simplified(Point<T> * p1, Point<T> * p2) {
        size_t dims = max(p1->getD(), p2->getD());
        double dist = 0;
        for (size_t i = 0; i < dims; i++) {
            double x = 0;
            double y = 0;
            if (p2->size() > i) {
                x = (*p1)[i];
            }
            if (p1->size() > i) {
                y = (*p2)[i];
            }
            double temp = pow(x - y, 2);
            dist = dist + temp;
        }
        return dist;
    }
};

#endif /* EUCLIDEANDISTANCE_H */


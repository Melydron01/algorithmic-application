#include "ExecInfo.h"

#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <fstream>


using namespace std;

ExecInfo::ExecInfo() {
    inputFile = NULL;
    outputFile = NULL;
    metric = CRMSD;
    inputFileFlag = false;
    outputFileFlag = false;
    metricFlag = false;
}

void ExecInfo::printHelp(char* progName) {
    cout << "Usage: " << progName << " [OPTION] ..." << endl << endl;
    cout << "\t-i <input file>" << endl;
    cout << "\t-f <metric>" << endl;
}

bool ExecInfo::argHandling(int argc, char** argv) {

    int i = 1;
    while (i < argc) {
        if (strcmp(argv[i], "-i") == 0) {
            if (inputFileFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            inputFile = argv[++i];
            inputFileFlag = true;
        } else if (strcmp(argv[i], "-f") == 0) {
            if (metricFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            if ((strcmp(argv[++i], "CRMSD") == 0) || (strcmp(argv[i], "cRMSD") == 0) || (strcmp(argv[i], "crmsd") == 0)) {
                metric = CRMSD;
            } else if ((strcmp(argv[i], "LIFD") == 0) || (strcmp(argv[i], "Frechet") == 0) || (strcmp(argv[i], "lifd") == 0)) {
                metric = LIFD;
            } else {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            metricFlag = true;
        }/* else if (strcmp(argv[i], "-complete") == 0) {
            if (completeFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            complete = true;
            completeFlag = true;
        }*/
        i++;
    }

    return true;
}

bool ExecInfo::fileExists(const char* fileName) {
    //    ifstream f(fileName);
    //    return f.good();


    if (FILE * file = fopen(fileName, "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}




#ifndef EXECINFO_H
#define EXECINFO_H

#include "Input.h"


class ExecInfo {
public:
    ExecInfo();
    bool argHandling(int argc, char**argv);
    bool fileExists(const char* fileName);
    void printHelp(char* progName);
    const char * inputFile;
    const char * outputFile;
    Function metric; // 1 - CRMSD , 2 - LIFD
    bool inputFileFlag;
    bool outputFileFlag;
    bool metricFlag;
private:

};


#endif /* EXECINFO_H */


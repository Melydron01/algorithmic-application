#ifndef INITIALIZATION_H
#define INITIALIZATION_H

#include "Centroid.h"
#include "Track.h"
#include "CentroidArray.h"

template <typename T>
class Initialization {
public:

    virtual ~Initialization() {

    }
    virtual CentroidArray<T> * initialize(Array_<T> * set, DistanceMatrix<TrackD * > * matrix, size_t k) = 0;
private:

};




#endif /* INITIALIZATION_H */


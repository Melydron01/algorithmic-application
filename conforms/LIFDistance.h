
#ifndef LIFDISTANCE_H
#define LIFDISTANCE_H

#include <algorithm>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <float.h>

#include "TrackDistance.h"
#include "Track.h"
#include "FrechetDistance.h"

using namespace std;
using namespace Eigen;

template <typename T>
class LIFDistance : public FrechetDistance<T> {
public:
    virtual double calculate(Track<T> * t1, Track<T> * t2) {
        int N = t1->size();

        double * dataX = t1->getColumnMajorMatrix();
        double * dataY = t2->getColumnMajorMatrix();

        // invert
        Map<MatrixXd> X(dataX, N, 3); // Nx3
        Map<MatrixXd> Y(dataY, N, 3); // Nx3

        // multiply
        MatrixXd XT = X.transpose(); // 3xN
        MatrixXd m = XT*Y; // 3xN * Nx3 = 3x3

        JacobiSVD<MatrixXd> svd(m, ComputeFullU | ComputeFullV);
        JacobiSVD<MatrixXd>::SingularValuesType singular = svd.singularValues();

        double s3 = singular(2);
        
//        cout << "  s3 : "  << s3 << endl;

        if (s3 > 0) {
            MatrixXd U = svd.matrixU();
            MatrixXd V = svd.matrixV();
            MatrixXd VT = V.transpose();
            MatrixXd Q = U*VT;

            double detQ = Q.determinant();

            if (detQ < 0) {
                U(0, 2) = -U(0, 2);
                U(1, 2) = -U(1, 2);
                U(2, 2) = -U(2, 2);

                Q = U*VT;
            }

            MatrixXd QX = X*Q;
            MatrixXd QXT = QX.transpose();
            MatrixXd YT = Y.transpose();

//            cout << "-------- QX " << endl;
//            cout << QX << endl;
//            
//            cout << "-------- QXT " << endl;
//            cout << QX.transpose() << endl;
//            
//            
//            cout << "-------- Y " << endl;
//            cout << Y << endl;
            
            double * data1 = QXT.data();
            double * data2 = YT.data();
            
//            for (int i=0;i<N*3;i++) {
//                cout << data1[i] << endl;
//            }
            
            Track<T> * t1 = new Track<T>(data1, N, 3) ;
            Track<T> * t2 = new Track<T>(data2, N, 3) ;

            double d2 = FrechetDistance<T>::calculate(t1, t2);

            return d2;
        } else {
            cout << "LIFDistance ############### ???? " << endl;
            return DBL_MAX;
        }
    }
};




#endif /* LIFDISTANCE_H */


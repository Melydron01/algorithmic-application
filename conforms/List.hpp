#include <stddef.h>
#include <iostream>
#include "List.h"

using namespace std;

// ------------------ LISTNODE ----------------

template<typename T>
ListNode<T>::ListNode(T data) : data(data) {

}

template<typename T>
ListNode<T>::~ListNode() {

}

template<typename T>
ListNode<T> * ListNode<T>::getNext() {
    return next;
}

template<typename T>
ListNode<T> * ListNode<T>::getPrevious() {
    return previous;
}

template<typename T>
void ListNode<T>::setNext(ListNode<T> * next) {
    this->next = next;
}

template<typename T>
void ListNode<T>::setPrevious(ListNode<T> * previous) {
    this->previous = previous;
}

template<typename T>
T ListNode<T>::getData() {
    return data;
}

template<typename T>
void ListNode<T>::setData(T data) {
    this->data = data;
}

// ------------------------------------------------------

template<typename T>
List<T>::List() : size(0), head(NULL) {
}

template<typename T>
List<T>::~List() {
    removeAll();
}

template<typename T>
void List<T>::printAll() {
    ListNode<T> * temp = head;
    while (temp != NULL) {
        cout << temp->getData();
        temp=temp->getNext();
    }
}

template<typename T>
void List<T>::insertFront(T t) {
    ListNode<T> * node = new ListNode<T>(t);
    node->setNext(head);    
    head = node;
    size++;
}

template<typename T>
int List<T>::getSize() {
    return size;
}

template<typename T>
void List<T>::removeAll() {
    ListNode<T> * temp = head;
    while (temp != NULL) {
        ListNode<T> * next = temp->getNext();
        delete temp;
        temp=next;
    }
    head = NULL;
}

template<typename T>
void List<T>::removeAllDeep() {
    ListNode<T> * temp = head;
    while (temp != NULL) {
        ListNode<T> * next = temp->getNext();
        delete temp->getData();
        delete temp;
        temp=next;
    }
    head = NULL;
}

template<typename T>
void List<T>::insertBack(T t) {
    ListNode<T> * node = new ListNode<T>(t);
    ListNode<T> * temp = head;
    while (temp != NULL) {
        temp=temp->getNext();
    }
    node->setNext(temp);
    size++;
}

template<typename T>
ListNode<T> * List<T>::getHead() {
    return head;
}

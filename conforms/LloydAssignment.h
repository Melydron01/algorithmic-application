#ifndef LLOYDASSIGNMENT_H
#define LLOYDASSIGNMENT_H

#include <cfloat>

#include "Assignment.h"
#include "CentroidItemInfo.h"

template <typename T>
class LloydAssignment : public Assignment<T> {
public:

    virtual ~LloydAssignment() {

    }

    virtual ItemCentroidInfo<T> * findItemCentroidInfo(T item, int index, CentroidArray<T> * centroids, DistanceMatrix<TrackD * > * matrix) {
        ItemCentroidInfo<T> * info = new ItemCentroidInfo<T>();
        info->bestDistance = DBL_MAX;
        info->secondbestDistance = DBL_MAX;
        info->bestCentroid = NULL;
        info->secondBestCentroid = NULL;
        info->index = index;

        for (size_t j = 0; j < centroids->size(); j++) {
//            cout << "Finding matrix[" << index << ", " << (*centroids)[j]->index << "]" << endl;
            double d = matrix->getDistance(index, (*centroids)[j]->index);
            //            double d = calculator->simplified(item, (*centroids)[j]->center);
            if (d < info->bestDistance) {
                info->secondbestDistance = info->bestDistance;
                info->bestDistance = d;
                info->secondBestCentroid = info->bestCentroid;
                info->bestCentroid = (*centroids)[j];
            } else if (d < info->secondbestDistance) {
                info->secondbestDistance = d;
                info->secondBestCentroid = (*centroids)[j];
            }
        }
        return info;
    }
    
    virtual Configuration<T> * assign(CentroidArray<T> * centroids, Array_<T> * items, DistanceMatrix<TrackD * > * matrix, bool findSecondBest = true) {
        Configuration<T> * config = new Configuration<T>();
        config->array = centroids;
        for (size_t i = 0; i < items->size(); i++) {
            ItemCentroidInfo<T> * info = findItemCentroidInfo((*items)[i], i, centroids, matrix);

            if (info->bestCentroid != NULL) {
                info->bestCentroid->items.push_back((*items)[i]);
                info->bestCentroid->item_infos.push_back(info);
            } else {
                printf("?? -- LloydAssignement \n"); //config->nearestItems.push_back(NULL);
            }
        }
        return config;
    }

private:

};

#endif /* LLOYDASSIGNMENT_H */


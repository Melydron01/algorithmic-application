#include "Loader.h"

#define ITEMS_TO_READ 756

using namespace std;

Loader::Loader() {
}

Loader::~Loader() {
}

Input * Loader::load(const char* inputFile) {
    Input * input = new Input();
    //    cout << "Opening inputFile: " << inputFile << endl;
    FILE * fp = fopen(inputFile, "r");
    input->d = 3;
    string track_id;
    char buf[100000];
    char * pch;
    int earlyFin = ITEMS_TO_READ;
    stringstream out;

//    input->lsh_K = 3;
//    input->L = 3;


    fgets(buf, sizeof buf, fp);
    int numConform = atoi(buf);
    if (numConform > ITEMS_TO_READ){
        input->numConform = ITEMS_TO_READ;
    }
    else{
        input->numConform = numConform;
    }
    fgets(buf, sizeof buf, fp);
    input->N = atoi(buf);
    input->maxM = input->N;

    input->trackSet = new TrackSet();
    PointD* point = NULL;

    for (int k = 0; k < input->numConform; k++) {
        if (k == earlyFin) {
            break;
        }
        out.str("");
        out << k;
        track_id = out.str();
        Track<double>* track = new Track<double>(track_id, input->N);

        for (int i = 0; i < input->N; i++) {
            fgets(buf, sizeof (buf), fp);
            point = new PointD(input->d);
            double c;
            pch = strtok(buf, "\t");
            for (int j = 0; j < input->d; j++) {
                c = strtold(pch, NULL);
                (*point)[j] = c;
                pch = strtok(NULL, "\t\n");
            }
            track->addPoint(point);
        }
        track->moveToCenter();
        input->trackSet->push_back(track);
//        cout << "TrackSet add: " << track->tag << endl;
    }
    fclose(fp);
    return input;

}

TrackSet * Loader::loadTracks(const char* inputFile) {
    TrackSet * trackSet = new TrackSet();

    FILE * fp = fopen(inputFile, "r");

    char buf[100000];
    string track_id;
    int track_size;
    int dims = 2;
    char* pch;
    while (fgets(buf, sizeof buf, fp) != NULL) {
        if (buf[0] == '@') {
            pch = strtok(buf, " \t");
            pch = strtok(NULL, "\n");
            dims = atoi(pch);
            //            cout << "@dimension " << dims << endl;
            continue;
        }
        //for each line (query), read tag and number of points
        pch = strtok(buf, "\t");
        track_id = pch;
        pch = strtok(NULL, "\t");
        track_size = atoi(pch);
        //        cout << "id: " << track_id << "\tsize: " << track_size;
        Track<double> * track = new Track<double>(track_id, track_size);
        int j = 0;
        double c;
        PointD * point = NULL;
        while (pch != NULL) {
            pch = strtok(NULL, " ,()\n");
            if (pch != NULL) {
                if (j == 0) {
                    point = new PointD(dims);
                }
                c = strtold(pch, NULL);
                (*point)[j] = c;
                j++;
                if (j == dims) {
                    j = 0;
                    track->addPoint(point);
                }
            }
        }
        trackSet->push_back(track);
        //        cout << ".";
    }
    fclose(fp);
    //    cout << "Loader finished" << endl;
    return trackSet;

}

void * Loader::loadConfig(const char* configFile) {
    FILE * fp = fopen(configFile, "r");
    int k, num_of_gc, L;
    num_of_gc = 2;
    L = 3;

    char buf[100000];
    char* pch;

    while (fgets(buf, sizeof buf, fp) != NULL) {
        pch = strtok(buf, " \t");
        //        while (pch != NULL) {

        if (strcmp(pch, "number_of_clusters:") == 0) {
            pch = strtok(NULL, " \n");
            k = atoi(pch);
        }
        if (strcmp(pch, "number_of_grid_curves:") == 0) {
            pch = strtok(NULL, " \n");
            num_of_gc = atoi(pch);
        }
        if (strcmp(pch, "number_of_hash_tables:") == 0) {

            pch = strtok(NULL, " \n");
            L = atoi(pch);
        }
        //        }

    }
    cout << "K = " << k << "\t Number of curves = " << num_of_gc << "\t L = " << L << endl;
    fclose(fp);
    return 0;
}

#ifndef LOADER_H
#define LOADER_H

#include "Track.h"
#include "Input.h"
#include "Input.h"
#include "TrackDistance.h"
#include <sstream>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


class Loader {
public:
    Loader();
    virtual ~Loader();

    TrackSet * loadTracks(const char* input);
    Input * load(const char* inputFile);
    Input * load(const char* inputFile, const char* configFile);
    void * loadConfig(const char* configFile);

};

#endif /* LOADER_H */

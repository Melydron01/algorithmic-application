#ifndef OUTPUT_H
#define OUTPUT_H

#include "Configuration.h"
#include "Silhouette.h"

class Output {
public:

    ~Output() {
        delete silhouette;
        delete config;
    }
    
    int init;
    int assign;
    int update;
    Function func;
    Configuration<TrackD*> * config;
    Evaluation* silhouette;
    double duration;
};



#endif /* OUTPUT_H */


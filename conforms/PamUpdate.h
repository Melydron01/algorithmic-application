#ifndef PAMUPDATE_H
#define PAMUPDATE_H

#include <cfloat>
#include <ctime>

#include "Update.h"
#include "Centroid.h"

template <typename T>
class PamUpdate : public Update<T> {
public:

    virtual ~PamUpdate() {

    }

    virtual CentroidArray<T> * update(Configuration<T> * configuration, DistanceMatrix<T> * matrix) {
        size_t K = configuration->array->size();
        size_t m_pos = 0;
        size_t t_pos = 0;
        double minJ = DBL_MAX;

        // for each cluster ...
        for (size_t i = 0; i < K; i++) { // for each centroid
            //            cout << "Checking cluster: " << i << endl;
            //            clock_t begin = clock();
            Centroid<T> * m = (*(configuration->array))[i];
            size_t m_items_size = m->items.size();
            //            cout << "Centroid has: " << m_items_size << " items" << endl;
            for (size_t j = 0; j < m_items_size; j++) { // for each non centroid in the specific centroid
                double local_dJ = 0;
                //                T t = m->items[j]; // pick the candidate for the swap
                int candidate_index = m->item_infos[j]->index;
                // calculate inner cost
                //                cout << "Calculating inner cost for item: " << j << endl;
                for (size_t inner_i = 0; inner_i < m_items_size; inner_i++) {
                    if (inner_i != j) {
                        //                        T item_in_same_cluster = m->items[inner_i];
                        int item_in_cluster_index = m->item_infos[inner_i]->index;

                        //                        T second_best_centroid = m->items[inner_i]
                        //                        double dit = calculator->simplified(item_in_same_cluster, t);
                        double dit = matrix->getDistance(item_in_cluster_index, candidate_index);
                        double dim = m->item_infos[inner_i]->bestDistance;
                        double dic = m->item_infos[inner_i]->secondbestDistance;


                        // ################
                        if (dit <= dic) {
                            local_dJ += dit - dim;
                        } else {
                            local_dJ += dic - dim;
                        }
                        // ################
                    }
                }
                //                cout << "Done." << endl;


                // calculate outer cost
                //                cout << "Calculating outer cost for item: " << j << endl;
                for (size_t outer_c = 0; outer_c < K; outer_c++) { // for each other cluster
                    if (outer_c != i) {
                        Centroid<T> * other_cluster = (*(configuration->array))[outer_c];

                        for (size_t outer_i = 0; outer_i < other_cluster->items.size(); outer_i++) {
                            //                            T item_in_other_cluster = other_cluster->items[outer_i];
                            int item_in_other_cluster_index = other_cluster->item_infos[outer_i]->index;
                            //                            double dit = calculator->simplified(item_in_other_cluster, t);
                            
//                            double dit = calculator->simplified(item_in_other_cluster, t, matrix);                            
                            double dit = matrix->getDistance(item_in_other_cluster_index, candidate_index);
                            double dic = other_cluster->item_infos[outer_i]->bestDistance;

                            if (dit < dic) {
                                local_dJ += dit - dic;
                            } else {
                                local_dJ += 0;
                            }
                        }
                    }
                }
                //                cout << "Done. " << endl;

                // calculate local_dJ

                if (local_dJ < minJ) {
                    minJ = local_dJ;
                    m_pos = i;
                    t_pos = j;

                }
            }
        }



        if (minJ < 0) {
            // apply swap
//            cout << "PAM swapping centroid for cluster: " << m_pos << " (minJ: " << minJ << ")" << endl;
            CentroidArray<T> * newCentroids = new CentroidArray<T>();

            for (size_t i = 0; i < K; i++) { // for each centroid
                Centroid<T> * m = (*(configuration->array))[i];
                if (i == m_pos) {
                    //                    cout << "swap: " << &(m->center) << " with " << t_pos << " " << &((*(configuration->array))[i]->items[t_pos]) << endl;
                    newCentroids->push_back(new Centroid<T>( (*(configuration->array))[i]->items[t_pos], (*(configuration->array))[i]->item_infos[t_pos]->index) );
                } else {
                    newCentroids->push_back(new Centroid<T>(m->center, m->index));
                }
            }
            delete configuration;
            return newCentroids;
        } else {
            return NULL;
        }
    }
};

#endif /* PAMUPDATE_H */


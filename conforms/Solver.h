#ifndef SOLVER_H
#define SOLVER_H

#include "Output.h"
#include "TrackDistance.h"
#include "EuclideanDistance.h"
#include "PointDistance.h"
#include "FrechetDistance.h"
#include "TimeWarpDistance.h"
#include "Printer.h"
#include "Initialization.h"
#include "KMeansInitialization.h"
#include "Assignment.h"
#include "LloydAssignment.h"
#include "Update.h"
#include "PamUpdate.h"
#include "Silhouette.h"

class Solver {
public:
    Solver(Input* input);
    ~Solver();

    Output* solve(bool verbose); // Kmeans - Lloyd - PAM 


private:
    Input* input;
    Printer printer;
    Configuration<TrackD*> * run(bool verbose, Initialization< TrackD * > * init, Assignment< TrackD * > * assignement, Update< TrackD* > * update);

};


#endif /* SOLVER_H */


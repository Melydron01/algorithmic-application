#ifndef TIMEWARPDISTANCE_H
#define TIMEWARPDISTANCE_H

#include <cfloat>

#include "PointDistance.h"

#define position(x,y,m) (((x) * (m)) + (y))

template <typename T>
class TimeWarpDistance : public TrackDistance<T> {
public:

    virtual ~TimeWarpDistance() {

    }

    virtual double calculate(Track<T> * t1, Track<T> * t2) {
        Array_<double> timeWarpTable;
        size_t m1 = t1->getLength();
        size_t m2 = t2->getLength();
        size_t n = m1 + 1;
        size_t m = m2 + 1;
        timeWarpTable.resize(n * m);
        for (size_t i = 1; i <= m1; i++) {
            timeWarpTable[position(i, 0, m)] = DBL_MAX;
        }
        for (size_t j = 1; j <= m2; j++) {
            timeWarpTable[position(0, j, m)] = DBL_MAX;
        }
        timeWarpTable[position(0, 0, m)] = 0.0;
        PointDistance<double> * eucl = new EuclideanDistance<double>();
        for (size_t i = 1; i <= m1; i++) {
            for (size_t j = 1; j <= m2; j++) {
                Point<T> * p = (*t1)[i - 1];
                Point<T> * q = (*t2)[j - 1];
                double distance = eucl->calculate(p, q);
                double c1 = timeWarpTable[position(i - 1, j, m)];
                double c2 = timeWarpTable[position(i, j - 1, m)];
                double c3 = timeWarpTable[position(i - 1, j - 1, m)];
                timeWarpTable[position(i, j, m)] = distance + min(min(c1, c2), c3);
            }
        }
        delete eucl;
        return timeWarpTable[position(m1, m2, m)];
    }

    virtual double simplified(Track<T> * t1, Track<T> * t2) {
        Array_<double> timeWarpTable;
        size_t m1 = t1->getLength();
        size_t m2 = t2->getLength();
        size_t n = m1 + 1;
        size_t m = m2 + 1;
        timeWarpTable.resize(n * m);
        for (size_t i = 1; i <= m1; i++) {
            timeWarpTable[position(i, 0, m)] = DBL_MAX;
        }
        for (size_t j = 1; j <= m2; j++) {
            timeWarpTable[position(0, j, m)] = DBL_MAX;
        }
        timeWarpTable[position(0, 0, m)] = 0.0;
        PointDistance<double> * eucl = new EuclideanDistance<double>();
        for (size_t i = 1; i <= m1; i++) {
            for (size_t j = 1; j <= m2; j++) {
                Point<T> * p = (*t1)[i - 1];
                Point<T> * q = (*t2)[j - 1];
                double distance = eucl->simplified(p, q);
                double c1 = timeWarpTable[position(i - 1, j, m)];
                double c2 = timeWarpTable[position(i, j - 1, m)];
                double c3 = timeWarpTable[position(i - 1, j - 1, m)];
                timeWarpTable[position(i, j, m)] = distance + min(min(c1, c2), c3);
            }
        }
        delete eucl;
        return timeWarpTable[position(m1, m2, m)];
    }
};

#endif /* TIMEWARPDISTANCE_H */


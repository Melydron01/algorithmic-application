#include "Track.h"
#include "Distance.h"

template<typename T>
Track<T>::Track() : tag("mT"), m(0) {
    //    this->resize(m);
}

template<typename T>
Track<T>::Track(string tag, int m) : tag(tag), m(m) {

    //    this->resize(m);
}

template<typename T>
Track<T>::~Track() {
    for (size_t i = 0; i<this->size(); i++) {
        delete (*this)[i];
    }
}

template<typename T>
size_t Track<T>::getLength() {
    return this->size();
}

template<typename T>
void Track<T>::addPoint(Point<T> * point, Distance< Point<T>* > * distCalculator) {
    if (this->size() > 0) {
        Point<T> * previousPoint = (*this)[this->size() - 1];

        if (point->equals_withinBound(previousPoint) == true) {
            delete point;
            return;
        }
    }
    this->push_back(point);
}

template<typename T>
void Track<T>::print() {
    //    cout << "Track " << tag << ", size: " << this->size() << ":" << endl;
    cout << "[";
    for (size_t i = 0; i<this->size(); i++) {
        if (i > 0)
            cout << " ";
        (*this)[i]->print();
    }
    cout << "]";
}

template<typename T>
bool Track<T>::equals(Track<T> * track) {
    if (track == NULL) {
        return false;
    }
    if (this->size() != track->size()) {
        return false;
    }
    for (size_t i = 0; i<this->size(); i++) {
        if (!(*this)[i]->equals((*track)[i])) {
            return false;
        }
    }
    return true;
}

template<typename T>
void Track<T>::moveToCenter() {
    if (this->size() == 0) {
        return;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    for (size_t d = 0; d < D; d++) {
        T xMean = 0;
        for (size_t i = 0; i < N; i++) {
            xMean = xMean + (*((*this)[i]))[d];
        }
        xMean /= N;

        for (size_t i = 0; i < N; i++) {
            (*((*this)[i]))[d] -= xMean;
        }
    }
}

template<typename T>
T * Track<T>::getRowMajorMatrix() {
    if (this->size() == 0) {
        return NULL;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    T * data = new T[N * D];

    size_t j = 0;

    for (size_t i = 0; i < N; i++) {
        for (size_t d = 0; d < D; d++) {
            data[j++] = (*((*this)[i]))[d];
        }
    }

    return data;
}

template<typename T>
T * Track<T>::getColumnMajorMatrix() {
    if (this->size() == 0) {
        return NULL;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    T * data = new T[N * D];

    size_t j = 0;

    for (size_t d = 0; d < D; d++) {
        for (size_t i = 0; i < N; i++) {
            data[j++] = (*((*this)[i]))[d];
        }
    }

    return data;
}

template<typename T>
Track<T>::Track(T * data, int m, int d) : tag("mT"), m(m) {
    int k =0;
    for (int i = 0; i < m; i++) {
        Point<T> * point = new Point<T>(d);
        for (int j = 0; j < d; j++) {
            (*point)[j] = data[k++];
        }
        addPoint(point);
    }
}
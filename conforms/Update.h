#ifndef UPDATE_H
#define UPDATE_H

#include "DistanceMatrix.h"


template <typename T>
class Update {
public:

    virtual ~Update() {

    }
    
    virtual CentroidArray<T> * update(Configuration<T> * configuration, DistanceMatrix<T> * matrix) = 0;
    
//    virtual CentroidArray<T> * update(Configuration<T> * configuration, Distance<T> * calculator) = 0;
private:

};

#endif /* UPDATE_H */


#include <cstdlib>
#include <cstring>
#include <iostream>
#include <time.h>
#include <valarray>
#include <iomanip>
#include "Loader.h"
#include "Solver.h"
#include "ExecInfo.h"
#include "Input.h"
#include "CRMSDDistance.h"
#include "LIFDistance.h"
#include "Output.h"

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SVD>

using namespace std;
using Eigen::MatrixXd;

int main(int argc, char** argv) {
    srand(time(0));
    cout << setprecision(8) << fixed;
    ExecInfo execInfo;
    if (!execInfo.argHandling(argc, argv)) {
        cerr << "Wrong arguments!!" << endl;
        execInfo.printHelp(argv[0]);
        return 0;
    }

    string temp;
    if (!execInfo.inputFileFlag) {
        cout << "Give input file path: ";
        getline(cin, temp);
        execInfo.inputFile = temp.c_str();
    }

    Loader * loader = new Loader();
    Input * input = loader->load(execInfo.inputFile);

    Distance<TrackD * > * trackDistCalculator;
    const char* distFunc;
    const char* outputFile;
    if (execInfo.metric == CRMSD) {
        distFunc = "CRMSD";
        outputFile = "./Outputs/crmsd.dat";
        trackDistCalculator = new CRMSDDistance<double>();
        input->func = CRMSD;
    } else {
        distFunc = "LIFD";
        outputFile = "./Outputs/frechet.dat";
        input->func = LIFD;
        trackDistCalculator = new LIFDistance<double>();
    }

    cout << "Input:\t" << execInfo.inputFile << endl;
    cout << "Output:\t" << outputFile << endl;
    string dataMatrixString;
    dataMatrixString = string(execInfo.inputFile) + "_" + distFunc + "_matrix.txt";
    const char* matrixFile = dataMatrixString.c_str();
    //    dataMatrix = execInfo.inputFile;
    //    cout << "dataMatrix file: " << dataMatrix << endl;


    input->matrix = new DistanceMatrix<TrackD *>(input->trackSet->size());

    if (execInfo.fileExists(matrixFile)) {
        cout << "Matrix file found. Will read pre-calculated distances." << endl;
        input->matrix->readFill(input->trackSet, trackDistCalculator, matrixFile);
        //        input->matrix->calcFill(input->trackSet, trackDistCalculator, matrixFile);
    } else {
        cout << "Matrix file not found. Will calculate and write all distances." << endl;
        input->matrix->calcFill(input->trackSet, trackDistCalculator, matrixFile);

    }

    input->hash = CLASSIC;
    input->complete = false;
    Printer printer;
    printer.print(input);
    //
    Solver *solver = new Solver(input);
    //
    Output* output = solver->solve(false);
    cout << endl;
    cout << "Done." << endl;
    cout << "Printing result to: " << outputFile << endl << endl;
    printer.print(output, outputFile);
    delete loader;
    delete solver;
    delete input;
    delete output;
    return 0;
}

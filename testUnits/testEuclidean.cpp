/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   testEuclidean.cpp
 * Author: eva
 *
 * Created on Nov 22, 2017, 4:54:34 PM
 */
#include <cmath>
#include "testEuclidean.h"
#include "../Distance.h"
#include "../EuclideanDistance.h"



CPPUNIT_TEST_SUITE_REGISTRATION(testEuclidean);

testEuclidean::testEuclidean() {
}

testEuclidean::~testEuclidean() {
}

void testEuclidean::setUp() {
}

void testEuclidean::tearDown() {
}

void testEuclidean::test1(){
    EuclideanDistance<double> calculator;
    PointD * a = new PointD(1,1);
    PointD * b = new PointD(1,3);
    double result = calculator.calculate(a, b);
    double num = sqrt(4);
    if (result == num){
        CPPUNIT_ASSERT(true);
    }
    else {
        CPPUNIT_ASSERT(false);
    }
}

void testEuclidean::test2(){
    EuclideanDistance<double> calculator;
    PointD * a = new PointD(1,1,2);
    PointD * b = new PointD(1,3,1);
    double result = calculator.calculate(a, b);
    double c = sqrt(5);
    if (result == c){
        CPPUNIT_ASSERT(true);
    }
    else {
        CPPUNIT_ASSERT(false);
    }
}


void testEuclidean::test3(){
    EuclideanDistance<double> calculator;
    PointD * a = new PointD(1,1,2,5);
    PointD * b = new PointD(1,3,2,9);
    double result = calculator.calculate(a, b);
    double c = sqrt(20);
    if (result == c){
        CPPUNIT_ASSERT(true);
    }
    else {
        CPPUNIT_ASSERT(false);
    }
}
void testEuclidean::testMethod() {
    CPPUNIT_ASSERT(true);
}

void testEuclidean::testFailedMethod() {
    CPPUNIT_ASSERT(false);
}

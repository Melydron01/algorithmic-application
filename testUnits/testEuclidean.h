
#ifndef TESTEUCLIDEAN_H
#define TESTEUCLIDEAN_H

#include <cppunit/extensions/HelperMacros.h>

class testEuclidean : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(testEuclidean);

    CPPUNIT_TEST(test1);
    CPPUNIT_TEST(test2);
    CPPUNIT_TEST(test3);
//    CPPUNIT_TEST(testMethod);
//    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    testEuclidean();
    virtual ~testEuclidean();
    void setUp();
    void tearDown();

private:
    void test1();
    void test2();
    void test3();
    
    void testMethod();
    void testFailedMethod();
};

#endif /* TESTEUCLIDEAN_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestFrechet.cpp
 * Author: eva
 *
 * Created on Nov 22, 2017, 5:40:15 PM
 */
#include "../Track.h"
#include "testFrechet.h"
#include "../Distance.h"
#include "../TrackDistance.h"
#include "../EuclideanDistance.h"


#include "../FrechetDistance.h"
//#include "../Point.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TestFrechet);

TestFrechet::TestFrechet() {
}

TestFrechet::~TestFrechet() {
}

void TestFrechet::setUp() {
}

void TestFrechet::tearDown() {
}

void TestFrechet::testMethod() {
    CPPUNIT_ASSERT(true);
}

void TestFrechet::testFailedMethod() {
    CPPUNIT_ASSERT(false);
}

void TestFrechet::test1() {
    Track<double> * t1 = new Track<double>("t1", 3);
    Track<double> * t2 = new Track<double>("t2", 3);

    PointD * a = new PointD(1, 1, 2);
    PointD * b = new PointD(4, 2, 1);
    PointD * c = new PointD(5, 2, 3);

    Distance<PointD*> * distCalculator = new EuclideanDistance<double>();

    t1->addPoint(a, distCalculator);
    t1->addPoint(b, distCalculator);
    t1->addPoint(c, distCalculator);

    PointD * d = new PointD(1, 1, 5);
    PointD * e = new PointD(2, 2, 3);
    PointD * f = new PointD(3, 3, 4);
    PointD * g = new PointD(4, 4, 5);
    PointD * h = new PointD(5, 5, 6);

    t2->addPoint(d);
    t2->addPoint(e);
    t2->addPoint(f);
    t2->addPoint(g);
    t2->addPoint(h);

    
    TrackDistance<double> * calculator = new FrechetDistance<double>();

    double result = calculator->calculate(t1, t2);

    double k = sqrt(18);
    if ((result - k) == 0) {
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }

}

void TestFrechet::test2() {
    Track<double> * t1 = new Track<double>("t1", 3);
    Track<double> * t2 = new Track<double>("t2", 3);

    PointD * a = new PointD(1, 1);
    PointD * b = new PointD(2, 1);
    PointD * c = new PointD(3, 1);

    Distance<PointD*> * distCalculator = new EuclideanDistance<double>();

    t1->addPoint(a, distCalculator);
    t1->addPoint(b, distCalculator);
    t1->addPoint(c, distCalculator);

    PointD * d = new PointD(1, 2);
    PointD * e = new PointD(2, 2);
    PointD * f = new PointD(3, 2);
//    PointD * g = new PointD(4, 4);
//    PointD * h = new PointD(5, 5);

    t2->addPoint(d);
    t2->addPoint(e);
    t2->addPoint(f);
//    t2->addPoint(g);
//    t2->addPoint(h);

    TrackDistance<double> * calculator = new FrechetDistance<double>();

    double result = calculator->calculate(t1, t2);

//    double k = sqrt(2);
    if ((result - 1) == 0) {
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestFrechet.h
 * Author: eva
 *
 * Created on Nov 22, 2017, 5:40:14 PM
 */

#ifndef TESTFRECHET_H
#define TESTFRECHET_H

#include <cppunit/extensions/HelperMacros.h>

class TestFrechet : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestFrechet);

    CPPUNIT_TEST(test1);
    CPPUNIT_TEST(test2);

    CPPUNIT_TEST_SUITE_END();

public:
    TestFrechet();
    virtual ~TestFrechet();
    void setUp();
    void tearDown();

private:
    void test1();
    void test2();
    void testMethod();
    void testFailedMethod();
};

#endif /* TESTFRECHET_H */


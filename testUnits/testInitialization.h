#ifndef TESTINITIALIZATIONCLASS_H
#define TESTINITIALIZATIONCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class TestInitializationClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestInitializationClass);

    CPPUNIT_TEST(test1);
//    CPPUNIT_TEST(testMethod);
//    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    TestInitializationClass();
    virtual ~TestInitializationClass();
    void setUp();
    void tearDown();

private:
    void test1();
    void testMethod();
    void testFailedMethod();
};

#endif /* TESTINITIALIZATIONCLASS_H */


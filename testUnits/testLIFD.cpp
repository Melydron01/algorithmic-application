/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   testLIFD.cpp
 * Author: eva
 *
 * Created on Jan 10, 2018, 4:54:04 PM
 */

#include "testLIFD.h"

#include "../Track.h"
#include "../Distance.h"
#include "../TrackDistance.h"
#include "../EuclideanDistance.h"

#include "../LIFDistance.h"

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <time.h>
#include <valarray> 
#include <iomanip>

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SVD>


CPPUNIT_TEST_SUITE_REGISTRATION(testLIFD);

testLIFD::testLIFD() {
}

testLIFD::~testLIFD() {
}

void testLIFD::setUp() {
}

void testLIFD::tearDown() {
}

void testLIFD::testMethod() {
    CPPUNIT_ASSERT(true);
}

void testLIFD::testFailedMethod() {
    CPPUNIT_ASSERT(false);
}

void testLIFD::test1() {
    Track<double> * t1 = new Track<double>("1", 5);
    Track<double> * t2 = new Track<double>("2", 5);

//    t1 = new Track<double>("1", 5);
    t1->addPoint(new Point<double>(1, 8, 3), NULL);
    t1->addPoint(new Point<double>(2, 1, 4), NULL);
    t1->addPoint(new Point<double>(3, 2, 5), NULL);
    t1->addPoint(new Point<double>(4, 3, 6), NULL);
    t1->addPoint(new Point<double>(5, 4, 7), NULL);

//    t2 = new Track<double>("2", 5);
    t2->addPoint(new Point<double>(6, 2, 3), NULL);
    t2->addPoint(new Point<double>(7, 3, 1), NULL);
    t2->addPoint(new Point<double>(8, 4, 2), NULL);
    t2->addPoint(new Point<double>(9, 5, 3), NULL);
    t2->addPoint(new Point<double>(1, 6, 4), NULL);

    t1->moveToCenter();
    t2->moveToCenter();

    LIFDistance<double> * calc = new LIFDistance<double>();
    
    double result = calc->calculate(t1, t2)  ;
    double num = 3.47715 ;
//    cout << "result: " << calc->calculate(t1, t2) << endl;

    if ( (result - num) < 0.001 ) {
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }
}

void testLIFD::test2() {
    Track<double> * t1 = new Track<double>("1", 4);
    Track<double> * t2 = new Track<double>("2", 4);

// t1 = new Track<double>("1", 4);
    t1->addPoint(new Point<double>(2, 8, 3), NULL);
    t1->addPoint(new Point<double>(2, 5, 4), NULL);
    t1->addPoint(new Point<double>(4, 2, 5), NULL);
    t1->addPoint(new Point<double>(6, 3, 4), NULL);

//    t2 = new Track<double>("2", 4);
    t2->addPoint(new Point<double>(3, 2, 3), NULL);
    t2->addPoint(new Point<double>(4, 6, 3), NULL);
    t2->addPoint(new Point<double>(8, 9, 2), NULL);
    t2->addPoint(new Point<double>(9, 7, 3), NULL);

    t1->moveToCenter();
    t2->moveToCenter();

    LIFDistance<double> * calc = new LIFDistance<double>();
    
    double result = calc->calculate(t1, t2)  ;
    double num = 1.08402 ;
//    cout << "result: " << calc->calculate(t1, t2) << endl;

    if ( (result - num) < 0.001 ) {
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }
}

void testLIFD::test3() {
    Track<double> * t1 = new Track<double>("1", 4);
    Track<double> * t2 = new Track<double>("2", 5);

// t1 = new Track<double>("1", 4);
    t1->addPoint(new Point<double>(2, 8, 3), NULL);
    t1->addPoint(new Point<double>(2, 5, 4), NULL);
    t1->addPoint(new Point<double>(4, 2, 5), NULL);
    t1->addPoint(new Point<double>(6, 3, 4), NULL);

//    t2 = new Track<double>("2", 4);
    t2->addPoint(new Point<double>(3, 2, 3), NULL);
    t2->addPoint(new Point<double>(4, 6, 3), NULL);
    t2->addPoint(new Point<double>(8, 9, 2), NULL);
    t2->addPoint(new Point<double>(9, 7, 3), NULL);
    t2->addPoint(new Point<double>(1, 6, 4), NULL);
    
    t1->moveToCenter();
    t2->moveToCenter();

    LIFDistance<double> * calc = new LIFDistance<double>();
    
    double result = calc->calculate(t1, t2)  ;
    double num = 2.82408 ;
//    cout << "result: " << calc->calculate(t1, t2) << endl;

    if ( (result - num) < 0.001 ) {
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }
}
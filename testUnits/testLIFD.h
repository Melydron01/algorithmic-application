/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   testLIFD.h
 * Author: eva
 *
 * Created on Jan 10, 2018, 4:53:53 PM
 */

#ifndef TESTLIFD_H
#define TESTLIFD_H

#include <cppunit/extensions/HelperMacros.h>

class testLIFD : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(testLIFD);

    CPPUNIT_TEST(test1);
    CPPUNIT_TEST(test2);
    CPPUNIT_TEST(test3);
    //    CPPUNIT_TEST(testMethod);
    //    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    testLIFD();
    virtual ~testLIFD();
    void setUp();
    void tearDown();

private:
     void test1();
     void test2();
     void test3();
    void testMethod();
    void testFailedMethod();
};

#endif /* TESTLIFD_H */


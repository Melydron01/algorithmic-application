#include <cstdlib>
#include <iostream>

#include "../Track.h"
#include "testTimeWarp.h"

#include "../Distance.h"
#include "../TrackDistance.h"
#include "../EuclideanDistance.h"


#include "../TimeWarpDistance.h"


CPPUNIT_TEST_SUITE_REGISTRATION(TestTimeWrap);

TestTimeWrap::TestTimeWrap() {
}

TestTimeWrap::~TestTimeWrap() {
}

void TestTimeWrap::setUp() {
}

void TestTimeWrap::tearDown() {
}

void TestTimeWrap::testMethod() {
    CPPUNIT_ASSERT(true);
}

void TestTimeWrap::testFailedMethod() {
    CPPUNIT_ASSERT(false);
}

void TestTimeWrap::test1() {
    Track<double> * t1 = new Track<double>("t1", 3);
    Track<double> * t2 = new Track<double>("t2", 3);

    PointD * a = new PointD(1, 2);
    PointD * b = new PointD(2, 3);
    PointD * c = new PointD(3, 4);

    t1->addPoint(a);
    t1->addPoint(b);
    t1->addPoint(c);

    PointD * d = new PointD(1, 1);
    PointD * e = new PointD(2, 2);
    PointD * f = new PointD(3, 3);

    t2->addPoint(d);
    t2->addPoint(e);
    t2->addPoint(f);

    TrackDistance<double> * calculator = new TimeWarpDistance<double>();


    double result = calculator->calculate(t1, t2);
    double k = 3.236;
//    double r = fabs(result - k);
    if ((result - k) < 0.001){
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }
}

void TestTimeWrap::test2() {
    Track<double> * t1 = new Track<double>("t1", 3);
    Track<double> * t2 = new Track<double>("t2", 3);

    PointD * a = new PointD(1, 2);
    PointD * b = new PointD(2, 3);
    PointD * c = new PointD(3, 4);

    t1->addPoint(a);
    t1->addPoint(b);
    t1->addPoint(c);

    PointD * d = new PointD(1, 2);
    PointD * e = new PointD(2, 3);
    PointD * f = new PointD(3, 4);

    t2->addPoint(d);
    t2->addPoint(e);
    t2->addPoint(f);

    TrackDistance<double> * calculator = new TimeWarpDistance<double>();


    double result = calculator->calculate(t1, t2);
    double k = 1.414;
    //    double r = fabs(result - k);
    if ((result - k) < 0.001) {
        //    if(true){
        CPPUNIT_ASSERT(true);
    } else {
        CPPUNIT_ASSERT(false);
    }
}
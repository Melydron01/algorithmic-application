/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestTimeWarp.h
 * Author: eva
 *
 * Created on Nov 22, 2017, 11:11:04 PM
 */

#ifndef TESTTIMEWRAP_H
#define TESTTIMEWRAP_H

#include <cppunit/extensions/HelperMacros.h>

class TestTimeWrap : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestTimeWrap);

    CPPUNIT_TEST(test1);
    CPPUNIT_TEST(test2);
    //    CPPUNIT_TEST(testMethod);
    //    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    TestTimeWrap();
    virtual ~TestTimeWrap();
    void setUp();
    void tearDown();

private:
    void test1();
    void test2();
    void testMethod();
    void testFailedMethod();
};

#endif /* TESTTIMEWRAP_H */


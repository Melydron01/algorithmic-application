#ifndef CENTROIDITEMINFO_H
#define CENTROIDITEMINFO_H

#include "Centroid.h"

template <typename T>
class Centroid;

template <typename T>
class ItemCentroidInfo {
public:
    int iteration;
    int index;
    double bestDistance;
    double secondbestDistance;
    Centroid<T> * bestCentroid;
    Centroid<T> * secondBestCentroid;
};


#endif /* CENTROIDITEMINFO_H */


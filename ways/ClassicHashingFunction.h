#ifndef CLASSIC_HASHINGFUNCTION_H
#define CLASSIC_HASHINGFUNCTION_H

#include "HashingFunction.h"


template <typename T>
class ClassicHashingFunction : public HashingFunction<T> {
public:
    int * r;
    const int M;
    int r_dimension;

    ClassicHashingFunction(int r_dimension) : M((1L << 32) - 5), r_dimension(r_dimension) {        
        r = new int[r_dimension];

        for (int i = 0; i < r_dimension; i++) {
            r[i] = (int) RandomGenerator::rangeIncl(0, RAND_MAX / 2.0);
        }
    }

    ~ClassicHashingFunction() {
        delete [] r;
    }

    virtual unsigned int hash(ProjectionVector<T> * v) {
        unsigned long long int res = 0;
        
        if ((size_t) r_dimension != v->size()) {
            cout << "r_dimension: " << r_dimension << "\t v->size(): " << v->size() << endl;
            cout << "!!Error!! in hash in Classic Hash Function" << endl;
        }
        
        for (int i=0;i<r_dimension;i++) {
            res += ((unsigned long long int)((*v)[i]*r[i])) % M;
        }
        
        res = res % M;
        
        return (unsigned int) res;
    }
};


#endif /* CLASSICHASHINGFUNCTION_H */


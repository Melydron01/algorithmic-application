#include "ExecInfo.h"

#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <fstream>


using namespace std;

ExecInfo::ExecInfo() {
    inputFile = NULL;
    outputFile = NULL;
    metric = DFT;
    inputFileFlag = false;
    outputFileFlag = false;
    metricFlag = false;
}

void ExecInfo::printHelp(char* progName) {
    cout << "Usage: " << progName << " [OPTION] ..." << endl << endl;
    cout << "\t-i <input file>" << endl;
    cout << "\t-f <metric>" << endl;
}

bool ExecInfo::argHandling(int argc, char** argv) {

    int i = 1;
    while (i < argc) {
        if ( (strcmp(argv[i], "-i") == 0) ) {
            if (inputFileFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            inputFile = argv[++i];
            inputFileFlag = true;
        } else if (strcmp(argv[i], "-f") == 0) {
            if (metricFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            if ((strcmp(argv[++i], "DFT") == 0) || (strcmp(argv[i], "dft") == 0) || (strcmp(argv[i], "frechet") == 0)) {
                metric = DFT;
            } else if ((strcmp(argv[i], "DTW") == 0) || (strcmp(argv[i], "dtw") == 0) || (strcmp(argv[i], "timewarp") == 0)) {
                metric = DTW;
            } else {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            metricFlag = true;
        } else if (strcmp(argv[i], "-s") == 0) {
            if (inputFileFlag) {
                cerr << "Wrong arguments!" << endl;
                return false;
            }
            inputFile = argv[++i];
            segment = true;
            inputFileFlag = true;
        }
        i++;
    }

    return true;
}

bool ExecInfo::fileExists(const char* fileName) {
    //    ifstream f(fileName);
    //    return f.good();


    if (FILE * file = fopen(fileName, "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

#ifndef FRECHETDISTANCE_H
#define FRECHETDISTANCE_H

#include <algorithm>

#include "TrackDistance.h"
#include "Track.h"

template <typename T>
class FrechetDistance : public TrackDistance<T> {
public:

    virtual ~FrechetDistance() {

    }

    double calculate(Track<T> * t1, Track<T> * t2) {
        Array_<double> frechetTable;
        size_t n = t1->getLength();
        size_t m = t2->getLength();
        frechetTable.resize(n * m);

        PointDistance<double> * eucl = new EuclideanDistance<double>();
        for (size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < m; j++) {

                if ((i == 0) && (j == 0)) {
                    frechetTable[i * m + j] = eucl->calculate((*t1)[i], (*t2)[j]);
                } else if (i == 0) {
                    frechetTable[i * m + j] = max(eucl->calculate((*t1)[i], (*t2)[j]), frechetTable[i * m + j - 1]);
                } else if (j == 0) {
                    frechetTable[i * m + j] = max(eucl->calculate((*t1)[i], (*t2)[j]), frechetTable[(i - 1) * m + j]);
                } else {
                    frechetTable[i * m + j] = max(eucl->calculate((*t1)[i], (*t2)[j]), min(min(frechetTable[(i - 1) * m + j], frechetTable[i * m + j - 1]), frechetTable[(i - 1) * m + j - 1]));
                }
            }
        }
        delete eucl;
        return frechetTable[n * m - 1];
    }

    double simplified(Track<T> * t1, Track<T> * t2) {
        Array_<double> frechetTable;
        size_t n = t1->getLength();
        size_t m = t2->getLength();
        frechetTable.resize(n * m);

        PointDistance<double> * eucl = new EuclideanDistance<double>();
        for (size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < m; j++) {

                if ((i == 0) && (j == 0)) {
                    frechetTable[i * m + j] = eucl->simplified((*t1)[i], (*t2)[j]);
                } else if (i == 0) {
                    frechetTable[i * m + j] = max(eucl->simplified((*t1)[i], (*t2)[j]), frechetTable[i * m + j - 1]);
                } else if (j == 0) {
                    frechetTable[i * m + j] = max(eucl->simplified((*t1)[i], (*t2)[j]), frechetTable[(i - 1) * m + j]);
                } else {
                    frechetTable[i * m + j] = max(eucl->simplified((*t1)[i], (*t2)[j]), min(min(frechetTable[(i - 1) * m + j], frechetTable[i * m + j - 1]), frechetTable[(i - 1) * m + j - 1]));
                }
            }
        }
        delete eucl;
        return frechetTable[n * m - 1];
    }
};

//  L := n * m table
//  for i = 1 to n do
//      for j = 1 to m do
//          if i = 1 and j = 1 then
//              L[i, j] := d(p1, q1)
//          else if i = 1 then
//              L[i, j] := max(d(p1, qj), L[1, j - 1])
//          else if j = 1 then
//              L[i, j] := max(d(pi, q1), L[i - 1, 1])
//          else
//              L[i, j] := max(d(pi, qj), min(L[i - 1, j], L[i, j - 1], L[i - 1, j - 1]))
//          end if
//      end for
//  end for
//  return L[n, m]

#endif /* FRECHETDISTANCE_H */


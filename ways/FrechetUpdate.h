#ifndef FRECHETUPDATE_H
#define FRECHETUPDATE_H

#include "Configuration.h"
#include "MeanFrechetCurve.h"
#include "DistanceMatrix.h"

template <typename T>
class FrechetUpdate : public Update<T> {
public:

    virtual ~FrechetUpdate() {

    }

    T findMean(Array_<T> * items, Distance<T> * calculator) {
        size_t totalLeaves = items->size();
        if (totalLeaves == 0) {
            //            cerr << "Empty cluster!" << endl;
            //            getchar();
            return NULL;
        }
        size_t totalNodes = 2 * totalLeaves - 1;
        size_t arraySize = totalNodes + 1;

        Array_<T> * heap = new Array_<T>(totalNodes + 1); // for guard

        for (size_t i = 0; i < totalLeaves; i++) {
            (*heap)[ arraySize - totalLeaves + i ] = (*items)[i];
        }

        size_t last_leaf_pos = arraySize - 1;
        //        cout << "findMean -- totalLeaves: " << totalLeaves << endl;
        //        cout << "totalNodes: " << totalNodes << endl;

        MeanFrechetCurve<double> mdftCalculator;

        for (int parent_pos = (int) (last_leaf_pos / 2); parent_pos > 0; parent_pos--) {
            //            cout << "parent: " << parent_pos << endl;
            int left_pos = parent_pos * 2;
            int right_pos = parent_pos * 2 + 1;

            if (right_pos <= (int) last_leaf_pos) {
                T leftitem = (*heap)[left_pos];
                T rightitem = (*heap)[right_pos];

                T p = mdftCalculator.meanCurve(leftitem, rightitem);
                (*heap)[parent_pos] = p;
            }
        }
        //        cout << "Done." << endl;
        T mean = (*heap)[1];
        //        size_t heap_size = heap->size();
        //        for (size_t i = 0; i < heap_size / 2; i++) {
        //            if ((*heap)[i] != mean)
        //                delete (*heap)[i];
        //        }
        return mean;
    }
    
    virtual CentroidArray<T> * update(Configuration<T> * configuration, DistanceMatrix<T> * matrix) {
        return NULL;
    }

    virtual CentroidArray<T> * update(Configuration<T> * configuration, Distance<T> * calculator) {
        CentroidArray<T> * newCentroids = new CentroidArray<T>();
        double threshold = 0.00001;

        int swaps = 0;
        // for each cluster ...
        for (size_t j = 0; j < configuration->array->size(); j++) {
            Centroid<T> * c = (*(configuration->array))[j];
            Centroid<T> * next;
            T mean = findMean(&c->items, calculator);

            if (mean != NULL) {
                double newdistance = calculator->simplified(c->center, mean);
                if (newdistance > threshold) {
                    swaps++;
                    next = new Centroid<T>(mean);
                }
                else
                    next = new Centroid<T>(c->center);
            }

            newCentroids->push_back(next);
        }

        if (swaps > 0) {
            cout << "MDF updating: " << swaps << " centroids" << endl;
            delete configuration;
            return newCentroids;
        } else {
            delete newCentroids;
            return NULL;
        }
    }
};

#endif /* FRECHETUPDATE_H */


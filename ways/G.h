#ifndef G_H
#define G_H

#include "H.h"
#include "GridTrack.h"
#include "ProjectionVector.h"

template<typename T>

class G {
public:
    G(int k, int d, int v_dimension, PointDistance<double> * pointDistanceCalc);
    virtual ~G();
    ProjectionVector<T> * lshForCurves(Track<T> * track);
    int get_k();
    int get_d();

    H<T> ** get_h();

private:
    int k;
    int d;
    H<T> ** h;
    PointDistance<double> * pointDistanceCalc;

};

#include "G.hpp"

#endif /* G_H */


#include "G.h"
#include "H.h"
#include "Config.h"

template<typename T>
G<T>::G(int k, int d, int v_dimension, PointDistance<double> * pointDistanceCalc) : k(k), d(d), pointDistanceCalc(pointDistanceCalc) {
    h = new H<T>*[k];
    double delta = RandomGenerator::rangeIncl(MIN_DELTA, MAX_DELTA);
    for (int i = 0; i < k; i++) {
        Grid<T> * grid = new Grid<T>(d, delta, pointDistanceCalc);
        h[i] = new H<T>(grid, v_dimension/k);
    }  
}

template<typename T>
G<T>::~G() {
    for (int i = 0; i < k; i++) {
        delete h[i];
    }
    delete [] h;
}

template<typename T>
ProjectionVector<T> * G<T>::lshForCurves(Track<T> * track) {
    ProjectionVector<T> * merged = new ProjectionVector<T>();

    for (int i = 0; i < k; i++) {
        ProjectionVector<T> * pv = h[i]->hash(track);
        // merge all pv with merged
        for (size_t j = 0; j < pv->size(); j++) {
            merged->push_back((*pv)[j]);
        }
        
        delete pv;
    }

    return merged;
}

template<typename T>
int G<T>::get_k() {
    return k;
}

template<typename T>
int G<T>::get_d() {
    return d;
}

template<typename T>
H<T> ** G<T>::get_h() {
    return h;
}

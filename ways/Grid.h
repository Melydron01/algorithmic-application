#ifndef GRID_H
#define GRID_H

#include "PointDistance.h"

template<typename T>
class Grid {
public:
    Grid(int d, double delta, PointDistance<double> * pointDistanceCalc);
    virtual ~Grid();
    Point<T> * nearestPoint(Point<T> * origin, bool verbose = false);
    int getD();
    void setD( int d);
    double getDelta();
    void setDelta(double delta);
    Array_<double> * get_t();
    PointDistance<double>* get_pointDistanceCalc();
private:
    int d; // grid dimensions
    double delta; // grid point distance in each dimension
    Array_<double> * t;
    PointDistance<double> * pointDistanceCalc;
};

#include "Grid.hpp"

#endif /* GRID_H */


#include <stddef.h>
#include <iostream>
#include <cmath>
#include <cfloat>
#include <cstdio>


#include "RandomGenerator.h"
#include "Array.h"
#include "Grid.h"
#include "PointDistance.h"
#include "EuclideanDistance.h"

using namespace std;

template<typename T>
Grid<T>::Grid(int d, double delta, PointDistance<double> * pointDistanceCalc) : d(d), delta(delta), pointDistanceCalc(pointDistanceCalc) {

    t = new Array_<T>(d);
    for (int i = 0; i < d; i++) {
        double x = RandomGenerator::rangeIncl(0, 1);
        (*t)[i] = x * 0.1;
    }
}

template<typename T>
Grid<T>::~Grid() {
    delete t;
}

template<typename T>
void Grid<T>::setDelta(double delta) {
    this->delta = delta;
}

template<typename T>
double Grid<T>::getDelta() {
    return this->delta;
}

template<typename T>
void Grid<T>::setD(int d) {
    this->d = d;
}

template<typename T>
int Grid<T>::getD() {
    return this->d;
}

template<typename T>
Point<T> * Grid<T>::nearestPoint(Point<T> * origin, bool verbose) {
    int d = this->getD();
    double delta = this->getDelta();
    if (verbose) {
        cout << "-- Grid delta: " << delta << endl;
        cout << "-- Grid t: ";
        size_t t_size = t->size();
        for (size_t i = 0; i < t_size - 1; i++) {
            cout << (*t)[i] << ", ";
        }
        cout << (*t)[t_size - 1] << endl << endl;
        cout << "Origin point: ";
        origin->print();
        cout << endl;
    }
    double components1[d];
    double components2[d];
    for (int i = 0; i < d; i++) {
        int x1, x2;
        x1 = floor((*origin)[i] / delta);
        x2 = ceil((*origin)[i] / delta);
        components1[i] = x1 * delta + (*t)[i];
        components2[i] = x2 * delta + (*t)[i];
    }
    Point<double> * gridPoint = new Point<double>(d);
    int check1[d] = {0};
    int check2[d] = {0};

    double minDistance = DBL_MAX;
    for (int i = 0; i < 1 << d; i++) {
        Point<double> * candidate = new Point<double>(d);
        for (int j = 0; j < d; j++) {
            if (check1[j] < 1 << (j)) {
                (*candidate)[j] = components1[j];
                check1[j]++;
            } else {
                (*candidate)[j] = components2[j];
                check2[j]++;
            }
            if (check2[j] == check1[j]) {
                check1[j] = 0;
                check2[j] = 0;
            }
        }
        if (verbose) {
            cout << "Candidate #" << i << ": ";
            candidate->print();
        }
        double candidateDistance = pointDistanceCalc->simplified(origin, candidate);
        if (verbose) {
            printf("C Distance: %.14lf\n", candidateDistance);
        }
        
        if (candidateDistance < minDistance) {
            for (int k = 0; k < d; k++) {
                (*gridPoint)[k] = (*candidate)[k];
            }
            minDistance = candidateDistance;
        }
        delete candidate;
    }
    return gridPoint;
}

template <typename T>
Array_<double> * Grid<T>::get_t() {
    return t;
}

template <typename T>
PointDistance<double>* Grid<T>::get_pointDistanceCalc() {
    return pointDistanceCalc;
}

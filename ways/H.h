#ifndef H_H
#define H_H

#include "Grid.h"
#include "List.h"
#include "GridTrack.h"
#include "ProjectionVector.h"

template <typename T>
class H {
public:
    H(Grid<T> * grid, int grid_v_dimension);
    virtual ~H();
    
    ProjectionVector<T> * hash(Track<T> * track);
    
private:
    ProjectionVector<T> * createProjectVector(GridTrack<T> * gt);
    
    Grid<T> * grid;
    int grid_v_dimension;
};

#include "H.hpp"

#endif /* H_H */


#ifndef H_HPP
#define H_HPP

#include "H.h"
#include "ProjectionVector.h"

template <typename T>
H<T>::H(Grid<T> * grid, int grid_v_dimension) : grid_v_dimension(grid_v_dimension) {
    this->grid = grid;
}

template <typename T>
H<T>::~H() {
    if (grid != NULL) {
        delete grid;
    }
}

template <typename T>
ProjectionVector<T> * H<T>::hash(Track<T> * track) {
    GridTrack<T> * gt = new GridTrack<T>();

    size_t points = track->size();

    for (size_t i = 0; i < points; i++) {
        Point<T>* trackPoint = (*track)[i];
        Point<T>* gridPoint = grid->nearestPoint(trackPoint);
        gt->addPoint(gridPoint);
    }

    ProjectionVector<T> * vector = createProjectVector(gt);

    delete gt;
    
    return vector;
}

template <typename T>
ProjectionVector<T> * H<T>::createProjectVector(GridTrack<T> * gt) {
    ProjectionVector<T> * vector = new ProjectionVector<T>();

    // TODO
    size_t gtLength = gt->getLength();
    for (size_t i = 0; i < gtLength; i++) {
        size_t dim = (*gt)[i]->getD();
        for (size_t j = 0; j < dim; j++) {
            double c = (*gt)[i]->at(j);
            vector->push_back(c);
        }

    }
    // pad with zeros
    while (vector->size() < (unsigned) grid_v_dimension) {
        vector->push_back(0);
    }

    return vector;
}

#endif /* H_HPP */


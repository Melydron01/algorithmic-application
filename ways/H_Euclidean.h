#ifndef H_EUCLIDEAN_H
#define H_EUCLIDEAN_H

#include "List.h"
#include "ProjectionVector.h"

template <typename T>
class H_Euclidean {
public:
    H_Euclidean(int v_dimension);
    
    unsigned int hash(ProjectionVector<T> * mergedvector);
    
private:
    unsigned int project(ProjectionVector<T> * v);
    
    Array_<double> v;
    double t;
    int W;
    int v_dimension;
};

#include "H_Euclidean.hpp"

#endif /* H_H */


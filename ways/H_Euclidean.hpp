/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   H.hpp
 * Author: melydron
 *
 * Created on October 13, 2017, 11:04 AM
 */

#ifndef H_EUCLIDEAN_HPP
#define H_EUCLIDEAN_HPP

#include "H_Euclidean.h"
#include "ProjectionVector.h"
#include "Config.h"

template <typename T>
H_Euclidean<T>::H_Euclidean(int v_dimension) : W(DEFAULT_W), v_dimension(v_dimension) {
    for (int i = 0; i < v_dimension; i++) {
        double x = RandomGenerator::normalDistribution();
        v.push_back(x);
    }

    t = RandomGenerator::rangeExcl(0, W);
}

template <typename T>
unsigned int H_Euclidean<T>::hash(ProjectionVector<T> * mergedvector) { // v t w
    double hashvalue = project(mergedvector);
    return (unsigned int) hashvalue;
}

template <typename T>
unsigned int H_Euclidean<T>::project(ProjectionVector<T> * p) {
    
    size_t R = p->size();
    double res = 0;
            
    for (size_t i = 0; i < R; i++) {
        res += (*p)[i]*v[i];
    }
    
    res += t;
    res /= W;

    return (unsigned int) res;
}


#endif /* H_HPP */


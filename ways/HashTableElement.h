/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HashTableElement.h
 * Author: melydron
 *
 * Created on October 25, 2017, 6:45 PM
 */

#ifndef HASHTABLEELEMENT_H
#define HASHTABLEELEMENT_H

#include "ProjectionVector.h"


template<typename T>
class HashTableElement {
public:

    HashTableElement(Track<T>* track, ProjectionVector<T>* projectionVector, int tracksetOffset) :
    track(track), projectionVector(projectionVector), visited(false), tracksetOffset(tracksetOffset) {
    }

    ~HashTableElement() {
        delete projectionVector;
    }

    Track<T>* track;
    ProjectionVector<T> * projectionVector;
    bool visited;
    int tracksetOffset;
};


#endif /* HASHTABLEELEMENT_H */


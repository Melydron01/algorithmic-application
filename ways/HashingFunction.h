#ifndef HASHINGFUNCTION_H
#define HASHINGFUNCTION_H

#include <vector>

#include "ProjectionVector.h"

using namespace std;

template <typename T>
class HashingFunction{
public:
    virtual ~HashingFunction(){
        
    }
    virtual unsigned int hash(ProjectionVector<T> * v) = 0;
};

#endif /* ARRAY_H */


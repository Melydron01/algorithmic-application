/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Hashtable.h
 * Author: melydron
 *
 * Created on October 10, 2017, 6:35 PM
 */

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include "G.h"
#include "TrackDistance.h"
#include "List.h"
#include "HashingFunction.h"
#include "HashTableElement.h"

template<typename T>
class Hashtable {
public:
    Hashtable(int k, int d, int v_dimension, int M, PointDistance<double> * pointDistanceCalc, HashingFunction<T> * hf);
    virtual ~Hashtable();

    void insert(Track<T> * track, int tracksetOffset);
    const int getSize();

    Track<T>* nearestNeighbour(Track<T> * track, TrackDistance<T>* distanceCalc, double & mindistance, bool &foundGridCurve);

    Array_<Track<T>*> * bucketList(int i);
    Array_<Track<T>*> * rangeSearch(Track<T> * track, double R, Distance<TrackD * > * distanceCalc);
    Array_<int> * rangeSearchOffsets(Track<T> * track, double R, Distance<TrackD * > * distanceCalc);


    void printBuckets();
private:
    G<T> * g;
    int k;
    int d;
    int v_dimension;
    const int TABLE_SIZE;

    PointDistance<double> * pointDistanceCalc;
    HashingFunction<T> * hf;
    List<HashTableElement<T>*> ** lists;
};

#include "Hashtable.hpp"

#endif /* HASHTABLE_H */


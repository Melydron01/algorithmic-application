#include <float.h>
#include <vector>

#include "List.h"
#include "HashingFunction.h"
#include "HashTableElement.h"
#include "Track.h"
#include "Hashtable.h"

template<typename T>
Hashtable<T>::Hashtable(int k, int d, int v_dimension, int M, PointDistance<double> * pointDistanceCalc, HashingFunction<T> * hf) : k(k), d(d), v_dimension(v_dimension), TABLE_SIZE(M), pointDistanceCalc(pointDistanceCalc), hf(hf) {
    g = new G<T>(k, d, v_dimension, pointDistanceCalc);

    lists = new List<HashTableElement<T>*>*[M];
    for (int i = 0; i < M; i++) {
        //        cout << "Hashtable list: " << i << endl;
        lists[i] = new List<HashTableElement<T>*>();
    }
}

template<typename T>
Hashtable<T>::~Hashtable() {
    delete g;

    for (int i = 0; i < TABLE_SIZE; i++) {
        lists[i]->removeAllDeep();
        delete lists[i];
    }

    delete [] lists;

    lists = NULL;
}

template<typename T>
void Hashtable<T>::insert(Track<T> * track, int tracksetOffset) {
    ProjectionVector<T> * mergedVector = g->lshForCurves(track);

    unsigned int offset = hf->hash(mergedVector) % TABLE_SIZE;

    HashTableElement<T> * element = new HashTableElement<T>(track, mergedVector, tracksetOffset);

    lists[offset]->insertFront(element);
}

template<typename T>
Track<T>* Hashtable<T>::nearestNeighbour(Track<T> * t1, TrackDistance<T>* distanceCalc, double & minDistance, bool &foundGridCurve) {
    ProjectionVector<T> * mergedVector = g->lshForCurves(t1);
    unsigned int offset = hf->hash(mergedVector) % TABLE_SIZE;
    foundGridCurve = false;
    minDistance = DBL_MAX;
    Track<T> * nearestNeighbour = NULL;

    for (ListNode< HashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        HashTableElement<T> * he = p->getData();
        Track<T> * t2 = he->track;
        ProjectionVector<T> * pv = he->projectionVector;

        if (mergedVector->equals(pv)) {
            foundGridCurve = true;
            he->visited = true;

            double tempDistance = distanceCalc->simplified(t1, t2);
            if (tempDistance < minDistance) {
                minDistance = tempDistance;
                nearestNeighbour = t2;
            }
        } else {
            he->visited = false;
        }
    }

    if (nearestNeighbour != NULL) {
        return nearestNeighbour;
    }

    for (ListNode< HashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        HashTableElement<T> * he = p->getData();
        Track<T> * t2 = he->track;

        if (he->visited == false) {
            double tempDistance = distanceCalc->simplified(t1, t2);
            if (tempDistance < minDistance) {
                minDistance = tempDistance;
                nearestNeighbour = t2;
            }
        }
    }
    delete mergedVector;
    return nearestNeighbour;
}
template<typename T>
const int Hashtable<T>::getSize(){
    return TABLE_SIZE;
}
template<typename T>
Array_<Track<T>*> * Hashtable<T>::bucketList(int i) {
    Array_<Track<T>*> * bucketList = new Array_<Track<T>*> ();
    Track<T> * addTrack = NULL;
    for (ListNode< HashTableElement<T>* > * p = lists[i]->getHead(); p != NULL; p = p->getNext()) {
        HashTableElement<T> * he = p->getData();
        addTrack = he->track;
        bucketList->push_back(addTrack);
    }
    return bucketList;
}

template<typename T >
Array_<Track<T>*> * Hashtable<T>::rangeSearch(Track<T> * track, double R, Distance<TrackD * > * distanceCalc) {
    Array_<Track<T>*> * neighbours = new Array_<Track<T>*> ();
    ProjectionVector<T> * mergedVector = g->lshForCurves(track);
    unsigned int offset = hf->hash(mergedVector) % TABLE_SIZE;
    Track<T> * checkTrack = NULL;
    for (ListNode< HashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        HashTableElement<T> * he = p->getData();
        checkTrack = he->track;

        double checkDistance = distanceCalc->simplified(track, checkTrack);
        if (checkDistance < R) {
            neighbours->push_back(checkTrack);
        }
    }
    delete mergedVector;
    return neighbours;
}

template<typename T >
Array_<int> * Hashtable<T>::rangeSearchOffsets(Track<T> * track, double R, Distance<TrackD * > * distanceCalc) {
    Array_<int> * neighbours = new Array_<int> ();
    ProjectionVector<T> * mergedVector = g->lshForCurves(track);
    unsigned int offset = hf->hash(mergedVector) % TABLE_SIZE;
    Track<T> * checkTrack = NULL;
    for (ListNode< HashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        HashTableElement<T> * he = p->getData();
        checkTrack = he->track;

        double checkDistance = distanceCalc->simplified(track, checkTrack);
        if (checkDistance < R) {
            neighbours->push_back(he->tracksetOffset);
        }
    }
    delete mergedVector;
    return neighbours;
}

template<typename T>
void Hashtable<T>::printBuckets() {
    cout << "Total Buckets: " << TABLE_SIZE << endl;
    for (int i = 0; i < TABLE_SIZE; i++) {
        int list_size = lists[i]->getSize();
        cout << "#" << i << " items: " << list_size << endl;
        //        cout << "----";
        //        ListNode < HashTableElement <T> *> * temp = lists[i]->getHead();
        //
        //        HashTableElement<T> * element;
        //        for (int j = 0; j < list_size; j++) {
        //            element = temp->getData();
        //            cout << element->track->tag << " ";
        //            temp = temp->getNext();
        //        }
        //        cout << endl;
    }

}

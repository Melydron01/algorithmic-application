#ifndef INPUT_H
#define INPUT_H

#include "Track.h"
#include "DistanceMatrix.h"

#include <vector>

using namespace std;

enum Function {
    DFT, DTW, CRMSD, LIFD
};

enum Hash {
    CLASSIC, PROBABILISTIC
};

class Input {
public:

    ~Input() {
        if (trackSet->size() > 0) {
            for (size_t i = 0; i < trackSet->size(); i++)
                delete (*trackSet)[i];
        }
    }
    int lsh_K; // K = 2
    int L; // L = 3
    int clusters_K;
    Function func; // CRMSD, LIFD
    Hash hash; // CLASSIC, PROBABILISTIC
    int d;
    int maxM;
    bool complete;
    TrackSet * trackSet;
    DistanceMatrix<TrackD * > * matrix;
    int numConform;
    int N;
private:
    int v_dimension;
};

#endif /* INPUT_H */


#ifndef KMEANSINITIALIZATION_H
#define KMEANSINITIALIZATION_H

#include <cfloat>
#include <vector>

#include "RandomGenerator.h"
#include "Distance.h"
#include "Centroid.h"
#include "DistanceMatrix.h"

template <typename T>
class KMeansInitialization : public Initialization<T> {
private:

    virtual ~KMeansInitialization() {

    }

    double calculateMinDistance(int item, CentroidArray<T> * array, DistanceMatrix<TrackD * > * matrix) {
        double m = DBL_MAX;
        size_t k = array->size();

        for (size_t i = 0; i < k; i++) {
//            double temp = calculator->simplified(item, (*array)[i]->center);
            double temp = matrix->getDistance(item, (*array)[i]->index);
            if (temp < m) {
                m = temp;
            }
        }
        return m;
    }
public:

    virtual CentroidArray<T> * initialize(Array_<T> * set, DistanceMatrix<TrackD * > * matrix, size_t k){ // Distance<T> * calculator, size_t k) {
        size_t N = set->size();

        if (k > N) {
            return NULL;
        }

        bool * selected = new bool[N]();

        CentroidArray<T> * array = new CentroidArray<T>();

        int index = 0;
        index = (int) RandomGenerator::rangeIncl(0, N - 1);
        //        Centroid<T> * centroid = new Centroid<T>((*set)[index]);
        Centroid<T> * centroid = new Centroid<T>((*set)[index], index);
        array->push_back(centroid);
        selected[index] = true;

        //        cout << "Item selected : " << index << endl;

        for (size_t i = 0; i < k - 1; i++) {
            double* pr = new double[N]();

            // calculate md for each candidate centroid

            double * md = new double[N]();

            double max = 0;
            for (size_t j = 0; j < N; j++) {
                md[j] = calculateMinDistance(j, array, matrix);
                if (md[j] > max) {
                    max = md[j];
                }
            }

            for (size_t j = 0; j < N; j++) {
                md[j] /= max;
            }

            //            cout << "Searching for centroid ... " << i << endl;
            for (size_t j = 0; j < N; j++) {
                if (selected[j]) {
                    if (j == 0) {
                        pr[j] = 0;
                    } else {
                        pr[j] = pr[j - 1];
                    }
                    continue;
                }

                if (j == 0) {
                    pr[j] = md[j];
                } else {
                    pr[j] = pr[j - 1] + md[j];
                }
            }

            //            for (size_t j = 0; j < N; j++) {
            //                cout << j << " " << pr[j] << endl;
            //            }

            double r;

            bool found = false;
            do {
                r = RandomGenerator::rangeExcl(0, pr[N - 1]);

                //                cout << "Random r = ... " << r << endl;

                for (size_t j = 0; j < N; j++) {
                    if (r < pr[j]) {
                        while ((j > 0 && fabs(pr[j] - pr[j - 1]) < DEFAULT_ERROR)) {
                            j++;
                        }
                        if (j < N) {
                            if (selected[j] == true) {
                                cout << "Error! [K-Means++ Initialization]" << endl;
                                exit(0);
                            }
                            //                            Centroid<T> * centroid = new Centroid<T>((*set)[j]);
                            Centroid<T> * centroid = new Centroid<T>((*set)[j], j);
                            array->push_back(centroid);
                            selected[j] = true;
                            //                            cout << "Item selected : " << j << endl;
                            found = true;
                            break;
                        }
                    }
                }
            } while (!found);

            delete [] md;
            delete [] pr;
        }

        delete [] selected;

        return array;

    }
};

#endif /* KMEANSINITIALIZATION_H */


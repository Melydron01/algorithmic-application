#ifndef LIFDISTANCE_H
#define LIFDISTANCE_H

#include <algorithm>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <float.h>

#include "FrechetDistance.h"

using namespace std;
using namespace Eigen;

template <typename T>
class LIFDistance : public TrackDistance<T> {
public:

    virtual ~LIFDistance() {

    }

    double simplified(Track<T> * t1, Track<T> *t2) {
        return 0;
    }

    double calculate(Track<T> * t1, Track<T> * t2) {
        int N = t1->size();
        int M = t2->size();
        Track<T> * tempT = NULL;
        ;
        if (N != M) {
            tempT = new Track<T>();
            int small, big;
            if (N > M) {
                big = N;
                small = M;
            } else {
                big = M;
                small = N;
            }
            int ignoreItems = big - small;
            int parts = ignoreItems + 1;
            int div = small / parts;
            int plusTimes = small % parts;
            int plusAmount = div + 1;
            int temp = 0;
            int i = big - 1;
            bool addItem;
            while (i >= 0) {
                addItem = false;
                if (plusTimes > 0) {
                    if (temp == plusAmount) {
                        plusTimes--;
                        temp = -1;
                    } else {
                        addItem = true;
                    }
                } else if (temp == div) {
                    temp = -1;
                } else {
                    addItem = true;
                }
                temp++;
                if (addItem) {
                    T x;
                    T y;
                    if (big == N) {
                        x = (*t1)[N - i - 1]->getX();
                        y = (*t1)[N - i - 1]->getY();
                    } else {
                        x = (*t2)[M - i - 1]->getX();
                        y = (*t2)[M - i - 1]->getY();
                    }
                    Point<T> * a = new Point<T>(x, y);
                    tempT->push_back(a);
                }
                i--;
            }
        }
        double * dataX;
        double * dataY;
        int matrixSize;
        if (N == M) {
            matrixSize = N;
            dataX = t1->getColumnMajorMatrix();
            dataY = t2->getColumnMajorMatrix();
        } else if (N > M) {
            matrixSize = M;
            dataX = tempT->getColumnMajorMatrix();
            dataY = t2->getColumnMajorMatrix();
        } else {
            matrixSize = N;
            dataX = t1->getColumnMajorMatrix();
            dataY = tempT->getColumnMajorMatrix();
        }

        // invert
        Map<MatrixXd> X(dataX, matrixSize, 2); // Nx2
        Map<MatrixXd> Y(dataY, matrixSize, 2); // Nx2
        // multiply
        MatrixXd XT = X.transpose(); // 2xN
        MatrixXd m = XT*Y; // 2xN * Nx2 = 2x2

        JacobiSVD<MatrixXd> svd(m, ComputeFullU | ComputeFullV);
        JacobiSVD<MatrixXd>::SingularValuesType singular = svd.singularValues();

        double s3 = singular(1);

        if (s3 > 0) {
            MatrixXd U = svd.matrixU();
            MatrixXd V = svd.matrixV();
            MatrixXd VT = V.transpose();
            MatrixXd Q = U*VT;

            double detQ = Q.determinant();

            if (detQ < 0) {
                U(0, 1) = -U(0, 1);
                U(1, 1) = -U(1, 1);
                Q = U*VT;
            }

            MatrixXd QX = X*Q;
            MatrixXd QXT = QX.transpose();
            MatrixXd YT = Y.transpose();
            double * data1 = QXT.data();
            double * data2 = YT.data();
            Track<T> * f1 = new Track<T>(data1, matrixSize, 2);
            Track<T> * f2 = new Track<T>(data2, matrixSize, 2);
            FrechetDistance<T> calc;
            double d2 = calc.calculate(f1, f2);
            delete f1;
            delete f2;
            if (tempT != NULL)
                delete tempT;
            return d2;
        } else {
            //                        cout << "LIFDistance ############### ???? " << endl;
            //            getchar();
            if (tempT != NULL)
                delete tempT;
            return DBL_MAX;
        }
    }
};




#endif /* LIFDISTANCE_H */


#include <cfloat>
#include <algorithm>

#include "LSH_Solver.h"
#include "Configuration.h"
#include "Input.h"
#include "Loader.h"
#include "ProbabilisticHashingFunction.h"
#include "ClassicHashingFunction.h"
#include "CentroidArray.h"
#include "FrechetDistance.h"
#include "MeanFrechetCurve.h"

#define DIVISOR (8)

LSH_Solver::LSH_Solver() : indexTables(NULL) {

}

LSH_Solver::~LSH_Solver() {

}

TrackD* findMean(Array_<TrackD*> * items, Distance<TrackD*> * calculator) {
    size_t totalLeaves = items->size();
    if (totalLeaves == 0) {
        //            cerr << "Empty cluster!" << endl;
        //            getchar();
        return NULL;
    }
    size_t totalNodes = 2 * totalLeaves - 1;
    size_t arraySize = totalNodes + 1;

    Array_<TrackD*> * heap = new Array_<TrackD*>(totalNodes + 1); // for guard

    for (size_t i = 0; i < totalLeaves; i++) {
        (*heap)[ arraySize - totalLeaves + i ] = (*items)[i];
    }

    size_t last_leaf_pos = arraySize - 1;
    //        cout << "findMean -- totalLeaves: " << totalLeaves << endl;
    //        cout << "totalNodes: " << totalNodes << endl;

    MeanFrechetCurve<double> mdftCalculator;

    for (int parent_pos = (int) (last_leaf_pos / 2); parent_pos > 0; parent_pos--) {
        //            cout << "parent: " << parent_pos << endl;
        int left_pos = parent_pos * 2;
        int right_pos = parent_pos * 2 + 1;

        if (right_pos <= (int) last_leaf_pos) {
            TrackD* leftitem = (*heap)[left_pos];
            TrackD* rightitem = (*heap)[right_pos];

            TrackD* p = mdftCalculator.meanCurve(leftitem, rightitem);
            (*heap)[parent_pos] = p;
        }
    }
    //        cout << "Done." << endl;
    TrackD* mean = (*heap)[1];
    //        size_t heap_size = heap->size();
    //        for (size_t i = 0; i < heap_size / 2; i++) {
    //            if ((*heap)[i] != mean)
    //                delete (*heap)[i];
    //        }
    return mean;
}

Configuration<TrackD*> * LSH_Solver::buildIndexTables(Input * input, int M, PointDistance<double>* pointDistanceCalc) {
    Configuration<TrackD*> * config = new Configuration<TrackD*>;
    this->input = input;
    int L = input->L;

    indexTables = new Array_<Hashtable<double>*>;
    TrackSet * tracks = input->trackSet;
    int temp = 1;
    int multiplier = 0;
    while (temp < int(input->trackSet->size())) {
        temp = temp << 1;
        multiplier++;
    }
    multiplier = 1;
    int v_dimension = input->lsh_K * input->maxM * input->d * multiplier;
    //    cout << "v_dimension: " << v_dimension << endl; 
    HashingFunction<double> * hf;

    if (input->hash == CLASSIC) {
        hf = new ClassicHashingFunction<double>(v_dimension);
    } else {
        hf = new ProbabilisticHashingFunction<double>(v_dimension);
    }

    for (int i = 0; i < input->L; i++) {
        //        cout << "K: " << input->K << " d: " << input->d << " v_dimension: " << v_dimension << " M: " << M << endl;
        Hashtable<double>* table = new Hashtable<double>(input->lsh_K, input->d, v_dimension, M, pointDistanceCalc, hf);
        indexTables->push_back(table);
        //        (*indexTables)[i] = new Hashtable<double>(input->K, input->d, v_dimension, M, pointDistanceCalc, hf);
    }
    // indexing

    //    cout << "****************************" << endl;
    //    cout << "Indexing..." << endl;
    for (size_t i = 0; i < tracks->size(); i++) {
        for (int j = 0; j < L; j++) {
            //            cout << "Inserting track: " << (*tracks)[i]->tag << " with offset: " << i << endl;
            (*indexTables)[j]->insert((*tracks)[i], i);
        }
        //        if (i % 500 == 0) {
        //            cout << "Progress: " << i << endl;
        //        }
    }
    //    Update< TrackD* > * update = new FrechetUpdate<TrackD *>();
    Distance<TrackD* > * calc = new FrechetDistance<double>();
    const int tableSize = (*indexTables)[0]->getSize();
    CentroidArray<TrackD*> * centroids = new CentroidArray<TrackD*>();

    for (int i = 0; i < tableSize; i++) {

        Array_<TrackD*> * bucketTracks = (*indexTables)[0]->bucketList(i);
        size_t bucketSize = bucketTracks->size();
        //        cout << "Found that bucket: " << i << " has " << bucketSize << " tracks" << endl;
        TrackD* mean = findMean(bucketTracks, calc);
        Centroid<TrackD*> * newCentroid = new Centroid<TrackD*>(mean);
        centroids->push_back(newCentroid);
        for (size_t j = 0; j < bucketSize; j++) {
            (*centroids)[i]->items.push_back((*bucketTracks)[j]);
        }
        //        cout << "Found mean with size: " << mean->getLength() << endl;
    }

    for (int i = 0; i < tableSize; i++) { // for each centroidArray
        size_t arraySize = (*centroids)[i]->items.size();
        Centroid<TrackD*> * center = (*centroids)[i];
        for (size_t j = 0; j < arraySize; j++) { // for each item in that Array
            TrackD* item = center->items[j];
            ItemCentroidInfo<TrackD*> * info = new ItemCentroidInfo<TrackD*>();
            info->bestDistance = calc->simplified(center->center, item);
            info->secondbestDistance = DBL_MAX;
            info->bestCentroid = (*centroids)[i];
            info->secondBestCentroid = NULL;
            for (int k = 0; k < tableSize; k++) { // for each other centroid
                if (i != k) {
                    double d = calc->simplified(item, (*centroids)[k]->center);

                    if (d < info->secondbestDistance) {
                        info->secondbestDistance = d;
                        info->secondBestCentroid = (*centroids)[k];
                    }
                }
            }
            center->item_infos.push_back(info);
        }
    }
    config->array = centroids;
    return config;
}

Output* LSH_Solver::solve(Input* input) {
    Output* output = new Output();
    input->L = 1;
    input->d = 2;
    input->lsh_K = 1;
    output->init = 2;
    int buckets = input->trackSet->size() / DIVISOR;
    PointDistance<double> * pointDistanceCalc = new EuclideanDistance<double>();
    Distance<TrackD * > * trackDistCalculator = new FrechetDistance<double>();
    cout << "Will check for optimal lsh_k in range: [1, 15]" << endl << endl;
    int noImprove = 0;
    double maxValue = -1;
    for (int i = 1; i < 15; i++) {
        input->lsh_K = i;
        clock_t begin = clock();
        Configuration<TrackD*> * config = buildIndexTables(input, buckets, pointDistanceCalc);
        clock_t end = clock();
        Silhouette<TrackD*> silhouette;
        Evaluation* eval = silhouette.calculate(config, trackDistCalculator);
        double value = eval->evaluationValue;
        if (value > maxValue) {
            noImprove = 0;
            maxValue = value;
            cout << "Current best Silhouette: " << value << "\t[lsh_k = " << i << "]" << endl;
            double duration = ((double) (end - begin) / CLOCKS_PER_SEC);
            output->silhouette = eval;
            output->duration = duration;
            output->config = config;
            output->update = i;
            if (value > 0.75) {
                char c;
                cout << "Found silhouette above 0.75. Stop checking? (y/n):";
                cin >> c;
                if (toupper(c) == 'Y') break;
            }
        } else {
            noImprove++;
            //            if (noImprove > 30) {
            //                noImprove = 0;
            //                char c;
            //                cout << "No improvement last 30 iterations [current k: " << i << "]. Stop checking? (y/n):";
            //                cin >> c;
            //                if (toupper(c) == 'Y') break;
            //            }
            delete config;
            delete eval;
        }

    }
    delete pointDistanceCalc;
    delete trackDistCalculator;
    return output;
}

void LSH_Solver::printTables() {
    for (int i = 0; i < input->L; i++) {
        (*indexTables)[i]->printBuckets();
    }
}

Array_<Track<double>*> * LSH_Solver::bucketTracks(int i) {
    Array_<Track<double>*>* bucketTracks = (*indexTables)[0]->bucketList(i);
    return bucketTracks;
}

//Array_<Track<double>*> * LSH_Solver::rangeNeighbours(Track<double> * track, double R, Distance<TrackD * > * distanceCalc) {
//    Array_<Track<double>*>* rangeNeighbours = new Array_<Track<double>*>;
//
//    size_t N = input->tracks->size();
//    bool * flags = new bool[N]();
//
//    for (int j = 0; j < input->L; j++) {
//        Array<int> * tableNeighbours = (*indexTables)[j]->rangeSearchOffsets(track, R, distanceCalc);
//
//        for (size_t i = 0; tableNeighbours->size(); i++) {
//            int offset = (*tableNeighbours)[i];
//
//            if (flags[offset] == false) {
//                flags[offset] = true;
//                rangeNeighbours->push_back((*input->tracks)[offset]);
//            }
//        }
//        delete tableNeighbours;
//    }
//    delete [] flags;
//
//    return rangeNeighbours;
//}
//
//Array_<int> * LSH_Solver::rangeNeighboursOffsets(Track<double> * track, double R, Distance<TrackD * > * distanceCalc) {
//    Array_<int>* rangeNeighbours = new Array_<int>;
//
//    size_t N = input->tracks->size();
//    bool * flags = new bool[N]();
//
//    for (int j = 0; j < input->L; j++) {
//        Array<int> * tableNeighbours = (*indexTables)[j]->rangeSearchOffsets(track, R, distanceCalc);
//
//        for (size_t i = 0; i < tableNeighbours->size(); i++) {
//            int offset = (*tableNeighbours)[i];
//
//            if (flags[offset] == false) {
//                flags[offset] = true;
//                rangeNeighbours->push_back(offset);
//            }
//        }
//        delete tableNeighbours;
//    }
//    delete [] flags;
//
//    return rangeNeighbours;
//}
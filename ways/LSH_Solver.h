#ifndef LSH_SOLVER_H
#define LSH_SOLVER_H

#include "Input.h"
#include "Output.h"
#include "Hashtable.h"
#include "TrackDistance.h"
#include "PointDistance.h"

class LSH_Solver {
public:
    LSH_Solver();
    virtual ~LSH_Solver();

    Configuration<TrackD*> * buildIndexTables(Input * input, int M, PointDistance<double>* pointDistanceCalc);
        Array_<Track<double>*> * bucketTracks(int i);

    //    Array_<Track<double>*> * rangeNeighbours(Track<double> * track, double R, Distance<TrackD * > * distanceCalc);
    //    Array_<int> * rangeNeighboursOffsets(Track<double> * track, double R, Distance<TrackD * > * distanceCalc);
    Output* solve(Input* input);
    void printTables();
    //    Output * solveBruteForce(Input * input, PointDistance<double> * pointDistanceCalc, TrackDistance<double> * distanceCalc);  
    //    Output * solveClassic(Input * input, int M, PointDistance<double> * pointDistanceCalc, TrackDistance<double> * distanceCalc);
private:
    Input * input;
    Array_<Hashtable<double>*>* indexTables;
};

#endif /* SOLVER_H */


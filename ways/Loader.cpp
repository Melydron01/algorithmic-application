#include "Loader.h"

#define ITEMS_TO_READ 756

using namespace std;

Loader::Loader() {
}

Loader::~Loader() {
}

Input * Loader::load(const char* inputFile) {
    Input * input = new Input();
    input->trackSet = new TrackSet();
    cout << "Opening inputFile: " << inputFile << endl;
    FILE * fp = fopen(inputFile, "r");
    input->d = 2;
    int dims = 2;
    string track_id;
    string old_id;
    int track_size;
    char buf[100000];
    char * pch;
    int earlyFin = ITEMS_TO_READ;
    input->maxM = 0;
    int k = 0;
    while (fgets(buf, sizeof buf, fp) != NULL) {
        //for each line (way), read id, old_id and size
        if (k++ == earlyFin) {
            break;
        }
        pch = strtok(buf, ", ");
        track_id = pch;
        pch = strtok(NULL, ", ");
        old_id = pch;
        pch = strtok(NULL, ", ");
        track_size = atoi(pch);
        //        cout << "id: " << track_id << "\tsize: " << track_size << endl;
        Track<double> * track = new Track<double>(track_id, track_size);
        int j = 0;
        double c;
        PointD * point = NULL;
        //        while (pch != NULL) {
        for (int i = 0; i < track_size; i++) {
            pch = strtok(NULL, ", \n");
            if (pch != NULL) {
                if (j == 0) {
                    point = new PointD(dims);
                }
                c = strtold(pch, NULL);
                (*point)[j] = c;
                j++;
                if (j == dims) {
                    j = 0;
                    track->addPoint(point);
                    //                    cout << " added point " << endl;
                }
            }
        }
        if (track->size() > 3) {
            track->moveToCenter();
            input->trackSet->push_back(track);
            if ((int) track->size() > input->maxM)
                input->maxM = track->size();
        } else delete track;
    }
    fclose(fp);
    return input;

}

TrackSet * Loader::loadTracks(const char* inputFile) {

    TrackSet * trackSet = new TrackSet();

    FILE * fp = fopen(inputFile, "r");

    char buf[100000];
    string track_id;
    string type;
    int dims = 2;

    PointDistance<double> * pointDistanceCalc = new EuclideanDistance<double>();
    SegHashingFunction<double> * hf = new SegHashingFunction<double>();
    Segmentator * segmentator = new Segmentator(pointDistanceCalc, hf);

    char* pch;
    while (fgets(buf, sizeof buf, fp) != NULL) {
        //for each line (query), read id and type
        pch = strtok(buf, ",");
        track_id = pch;
        pch = strtok(NULL, ",");
        type = pch;
        //        cout << "id: " << track_id << "\tsize: " << track_size;
        Track<double> * track = new Track<double>(track_id, 0);
        int j = 0;
        double c;
        PointD * point = NULL;
        while (pch != NULL) {
            pch = strtok(NULL, " ,\n");
            if (pch != NULL) {
                if (j == 0) {
                    point = new PointD(dims);
                }
                c = strtold(pch, NULL);
                (*point)[j] = c;
                j++;
                if (j == dims) {
                    j = 0;
                    track->addPoint(point);
                }
            }
        }
        segmentator->insert(trackSet, track);
    }
    fclose(fp);
    //    cout << "Loader finished" << endl;
    TrackSet * newTrackSet = new TrackSet();
    for (size_t k = 0; k < trackSet->size(); k++) {

        TrackD* track = (*trackSet)[k];
        Track<double> * newTrack = new Track<double>(track->tag, 0);
        size_t trackSize = track->getLength();
        int temp = 0;
        PointD* p1;
        PointD* p2;
        PointD* p3;
        for (size_t i = 0; i < trackSize; i++) {
            temp++;
            PointD* point = new PointD((*track)[i]->getX(), (*track)[i]->getY());
            if (temp == 1) {
                p1 = point;
                newTrack->addPoint(point);
            } else if (temp == 2) {
                p2 = point;
                newTrack->addPoint(point);
            } else {
                if (temp == 3) {
                    p3 = point;
                } else {
                    p1 = p2;
                    p2 = p3;
                    p3 = point;
                }
                if (segmentator->angleSeg(p1, p2, p3)) {
                    temp = 2;
                    newTrackSet->push_back(newTrack);
                    char temp[100];
                    sprintf(temp, "%d", rand() % 1000);
                    string s = newTrack->tag + "_" + temp;

                    newTrack = new TrackD(s, 0);
                    PointD* newP1 = new PointD(p2->getX(), p2->getY());
                    newTrack->push_back(newP1);
                    PointD* newP2 = new PointD(p3->getX(), p3->getY());
                    newTrack->push_back(newP2);
                    p1 = newP1;
                    p2 = newP2;
                } else {
                    newTrack->addPoint(point);
                }
            }
        }
        newTrackSet->push_back(newTrack);


        //        //        cout << "--- Original ---" <<endl;
        //        cout << "#" << k << " ";
        //        (*trackSet)[k]->print();
        //        cout << endl;
        //        //        (*trackSet)[k]->moveToCenter();
        //        //        cout << "---  Moved  ---" <<endl;
        //        //        (*trackSet)[k]->print();
        //        //        cout << endl;
    }
    cout << "trackSet size: " << trackSet->size() << " (after intersection segmentation)" << endl;
    cout << "trackSet size: " << newTrackSet->size() << " (after angle segmentation)" << endl;
    delete trackSet;
    return newTrackSet;

}

void * Loader::loadConfig(const char* configFile) {
    FILE * fp = fopen(configFile, "r");
    int k, num_of_gc, L;
    num_of_gc = 2;
    L = 3;

    char buf[100000];
    char* pch;

    while (fgets(buf, sizeof buf, fp) != NULL) {
        pch = strtok(buf, " \t");
        //        while (pch != NULL) {

        if (strcmp(pch, "number_of_clusters:") == 0) {
            pch = strtok(NULL, " \n");
            k = atoi(pch);
        }
        if (strcmp(pch, "number_of_grid_curves:") == 0) {
            pch = strtok(NULL, " \n");
            num_of_gc = atoi(pch);
        }
        if (strcmp(pch, "number_of_hash_tables:") == 0) {

            pch = strtok(NULL, " \n");
            L = atoi(pch);
        }
        //        }

    }
    cout << "K = " << k << "\t Number of curves = " << num_of_gc << "\t L = " << L << endl;
    fclose(fp);
    return 0;
}

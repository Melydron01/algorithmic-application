#ifndef LSHASSIGNMENT_H
#define LSHASSIGNMENT_H

#include "Assignment.h"
#include "LSH_Solver.h"
#include "CentroidItemInfo.h"

template <typename T>
class LshAssignment : public Assignment<T> {
public:

    LshAssignment(LSH_Solver * solver) : solver(solver) {

    }

    virtual ~LshAssignment() {

    }

    virtual Configuration<T> * assign(CentroidArray<T> * centroids, Array_<T> * items, DistanceMatrix<TrackD * > * matrix, bool findSecondBest = true) {
    }

//    virtual Configuration<T> * assign(CentroidArray<T> * centroids, Array<T> * items, Distance<T> * calculator, bool findSecondBest = true) {
//        Configuration<T> * config = new Configuration<T>();
//
//        config->array = centroids;
//        //clear previous cluster items
//        //        size_t cSize = centroids->size();
//        //        for (size_t i = 0; i < cSize; i++) {
//        //            (*centroids)[i]->items.clear();
//        //            (*centroids)[i]->item_infos.clear();
//        //        }
//        double R = findFirstR(centroids, calculator);
//
//        size_t N = items->size();
//
//        Array<ItemCentroidInfo<T> * > infos(N);
//
//        for (size_t i = 0; i < N; i++) {
//            ItemCentroidInfo<T> * info = new ItemCentroidInfo<T>();
//            info->iteration = -1;
//            info->bestDistance = DBL_MAX;
//            info->secondbestDistance = DBL_MAX;
//            info->bestCentroid = NULL;
//            info->secondBestCentroid = NULL;
//            infos[i] = info;
//            //            infos.push_back(info);
//        }
//
//        int iteration = 0;
//        int changes = 0;
//        int threshold = 0;
//
//        do {
//            changes = 0;
//            for (size_t i = 0; i < centroids->size(); i++) { // for each cluster centroid
//                Array<int> * results = solver->rangeNeighboursOffsets((*centroids)[i]->center, R, calculator); // find r neighbours
//                //                cout << "Centroid: " << i << " rangeNeighboursOffsets size:" << results->size() << endl;
//
//                // update infos
//                for (size_t j = 0; j < results->size(); j++) {
//                    int offset = (*results)[j];
//                    if (infos[offset]->iteration == -1 || infos[offset]->iteration == iteration) {
//                        T item = (*items)[offset];
//                        bool checkIfCentroid = false;
//                        for (size_t k = 0; k < centroids->size(); k++) {
//                            if (item == (*centroids)[k]->center)
//                                checkIfCentroid = true;
//                        }
//                        if (!checkIfCentroid) {
//                            double d = calculator->simplified(item, (*centroids)[i]->center);
//                            if (d < infos[offset]->bestDistance) {
//                                infos[offset]->secondbestDistance = infos[offset]->bestDistance;
//                                infos[offset]->bestDistance = d;
//                                infos[offset]->secondBestCentroid = infos[offset]->bestCentroid;
//                                infos[offset]->bestCentroid = (*centroids)[i];
//
//                                changes++; // update changes
//                            } else if (d < infos[offset]->secondbestDistance) {
//                                infos[offset]->secondbestDistance = d;
//                                infos[offset]->secondBestCentroid = (*centroids)[i];
//                                changes++; // update changes
//                            }
//                            infos[offset]->iteration = iteration;
//
//                        }
//                    }
//                }
//                delete results;
//            }
//            iteration++;
//
//            R = R * 2;
//        } while (changes > threshold);
//
//
//        // assign with Lloyd for unassigned items
//        for (size_t offset = 0; offset < N; offset++) {
//            if (infos[offset]->iteration == -1) {
//                T item = (*items)[offset];
//                for (size_t j = 0; j < centroids->size(); j++) {
//                    double d = calculator->simplified(item, (*centroids)[j]->center);
//                    if (d < infos[offset]->bestDistance) {
//                        infos[offset]->secondbestDistance = infos[offset]->bestDistance;
//                        infos[offset]->bestDistance = d;
//                        infos[offset]->secondBestCentroid = infos[offset]->bestCentroid;
//                        infos[offset]->bestCentroid = (*centroids)[j];
//                        changes++; // update changes
//                    } else if (d < infos[offset]->secondbestDistance) {
//                        infos[offset]->secondbestDistance = d;
//                        infos[offset]->secondBestCentroid = (*centroids)[j];
//                        changes++; // update changes
//                    }
//                }
//                infos[offset]->iteration = -2;
//            }
//        }
//
//        // add all items to their centroids
//        for (size_t i = 0; i < N; i++) {
//            if (infos[i]->iteration == -1) {
//                cerr << "Something went really wrong..." << endl;
//                exit(0);
//            }
//
//            //            if (infos[i]->secondBestCentroid == NULL) {
//            if (infos[i]->iteration != -2) {
//                T item = (*items)[i];
//
//                double secondbestDistance = DBL_MAX;
//
//                for (size_t other_c = 0; other_c < centroids->size(); other_c++) { // for each cluster
//                    Centroid<T> * other_cluster = (*centroids)[other_c];
//
//                    if (other_cluster != infos[i]->bestCentroid) { // if it's different that item's cluster
//                        double d = calculator->simplified(item, other_cluster->center);
//
//                        if (d < secondbestDistance) {
//                            secondbestDistance = d;
//                            infos[i]->secondBestCentroid = other_cluster;
//                        }
//                    }
//                }
//                infos[i]->secondbestDistance = secondbestDistance;
//            }
//
//            if (infos[i]->bestCentroid != NULL) {
//                infos[i]->bestCentroid->items.push_back((*items)[i]);
//                infos[i]->bestCentroid->item_infos.push_back(infos[i]);
//                //                config->nearestItems.push_back(infos[i]->bestCentroid->center);
//            } else {
//                cerr << "Something went really wrong..." << endl;
//                exit(0);
//            }
//        }
//        return config;
//    }

private:
    LSH_Solver * solver;

    double findFirstR(CentroidArray<T> * centroids, Distance<T> * calculator) {
        double min = DBL_MAX;

        for (size_t i = 0; i < centroids->size(); i++) {
            for (size_t j = i + 1; j < centroids->size(); j++) {
                double distance = calculator->simplified((*centroids)[i]->center, (*centroids)[j]->center);
                if (distance < min) {
                    min = distance;
                }
            }
        }
        double R = min / 2.0;

        return R;
    }
};

#endif /* LSHASSIGNMENT_H */


#ifndef MEANFRECHETCURVE_H
#define MEANFRECHETCURVE_H

#include <algorithm>

#include "TrackDistance.h"
#include "Track.h"

template <typename T>
class Pair {
public:
    T left;
    T right;

    Pair(T left, T right) : left(left), right(right) {

    }
};

template <typename T>
class MeanFrechetCurve {
public:

    virtual ~MeanFrechetCurve() {
    }

    Track<T> * meanCurve(Track<T> * t1, Track<T> * t2) {
        if (t1 == NULL) {
            cerr << "T1 was NULL, terminating!" << endl;
            exit(0);
        }
        if (t2 == NULL) {
            return t1;
        }
        Array_<double> frechetTable;
        size_t n = t1->getLength();
        size_t m = t2->getLength();
        frechetTable.resize(n * m);
        //        cout << "Creating frechetTable, t1.size: " << n << ", t2.size: " << m << endl; 
        PointDistance<double> * distanceCalc = new EuclideanDistance<double>();
        for (size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < m; j++) {
                if ((i == 0) && (j == 0)) {
                    frechetTable[i * m + j] = distanceCalc->simplified((*t1)[i], (*t2)[j]);
                } else if (i == 0) {
                    frechetTable[i * m + j] = max(distanceCalc->simplified((*t1)[i], (*t2)[j]), frechetTable[i * m + j - 1]);
                } else if (j == 0) {
                    frechetTable[i * m + j] = max(distanceCalc->simplified((*t1)[i], (*t2)[j]), frechetTable[(i - 1) * m + j]);
                } else {
                    frechetTable[i * m + j] = max(distanceCalc->simplified((*t1)[i], (*t2)[j]), min(min(frechetTable[(i - 1) * m + j], frechetTable[i * m + j - 1]), frechetTable[(i - 1) * m + j - 1]));
                }
            }
        }
        Array_<Pair<size_t> > traversal;
        size_t Pi = n - 1;
        size_t Qi = m - 1;
        traversal.push_back(Pair<size_t>(Pi, Qi));
        // calculate steps
        while (Pi > 0 && Qi > 0) {
            size_t minIdx = indexOfMin(frechetTable, m, Pi, Qi);
            switch (minIdx) {
                case 0:
                    traversal.push_back(Pair<size_t>(--Pi, Qi));
                    break;
                case 1:
                    traversal.push_back(Pair<size_t>(Pi, --Qi));
                    break;
                case 2:
                    traversal.push_back(Pair<size_t>(--Pi, --Qi));
                    break;
                default:
                    cerr << "minIndx ??" << endl;
                    exit(0);
            }
        }
        while (Pi > 0) {
            traversal.push_back(Pair<size_t>(--Pi, Qi));
        }
        while (Qi > 0) {
            traversal.push_back(Pair<size_t>(Pi, --Qi));
        }
        // calculate mean for each pair
        size_t t_size = traversal.size();
        Track<T>* meanTrack = new Track<T>();
        int mc_size;
        if (n > m) {
            mc_size = (int) n;
        } else
            mc_size = (int) m;
        int ignoreItems = (int) (t_size - mc_size);
        int parts = ignoreItems + 1;
        int div = mc_size / parts;
        int plusTimes = mc_size % parts;
        int plusAmount = div + 1;
        int temp = 0;
        int i = (int) (t_size - 1);
        bool addItem;
        while (i >= 0) {
            addItem = false;
            if (plusTimes > 0) {
                if (temp == plusAmount) {
                    plusTimes--;
                    temp = -1;
                } else {
                    addItem = true;
                }
            } else if (temp == div) {
                temp = -1;
            } else {
                addItem = true;
            }
            temp++;
            if (addItem) {
                Point<T> * a = (*t1)[traversal[i].left];
                Point<T> * b = (*t2)[traversal[i].right];
                Point<T> * c = new Point<T>(a->getD());
                if (a == NULL || b == NULL) {
                    cerr << "null traversal points error" << endl;
                    exit(0);
                } else {
                    for (size_t j = 0; j < a->getD(); j++) {
                        (*c)[j] = ((*a)[j]+(*b)[j]) / 2.0;
                    }
                }
                meanTrack->push_back(c);
            }
            i--;
        }
        //        cout << "traversal: " << t_size << "\tmc: " << mc_size << "\tmeanTrack: " << meanTrack->size() << "\tstep: " << step << endl;
        delete distanceCalc;
        return meanTrack;
    }

private:
    int indexOfMin(Array_<double> & frechetTable, int m, int Pi, int Qi) {
        double value0 = frechetTable[(Pi - 1) * m + Qi];
        double value1 = frechetTable[(Pi) * m + Qi - 1];
        double value2 = frechetTable[(Pi - 1) * m + Qi - 1];

        if (value0 < value1) {
            if (value0 < value2) {
                return 0;
            } else {
                return 2;
            }
        } else {
            if (value1 < value2) {
                return 1;
            } else {
                return 2;
            }
        }
    }

};

#endif /* MEANFRECHETCURVE_H */
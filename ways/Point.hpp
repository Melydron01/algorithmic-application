#include "Point.h"

#include <cstdio>
#include <cmath>
#include <iomanip>

template <typename T>
Point<T>::Point(int d) : Array_<T>(d) {
}

template <typename T>
Point<T>::Point(T x, T y) : Array_<T>(2) {
    (*this)[0] = x;
    (*this)[1] = y;
}

template <typename T>
Point<T>::Point(T x, T y, T z) : Array_<T>(3) {
    (*this)[0] = x;
    (*this)[1] = y;
    (*this)[2] = z;
}

template <typename T>
Point<T>::Point(T x, T y, T z, T w) : Array_<T>(4) {
    (*this)[0] = x;
    (*this)[1] = y;
    (*this)[2] = z;
    (*this)[3] = w;
}

template <typename T>
size_t Point<T>::getD() {
    return this->size();
}

template <typename T>
void Point<T>::print() {
    //    cout << "(";
    for (size_t i = 0; i<this->size(); i++) {
        if (i > 0) {
            cout << ", ";
        }
        //        cout << setprecision(7) << fixed;
        cout << (*this)[i];
        //        printf("%.3lf", (*this)[i]);
    }
    //    cout << ") " << " " << this << endl;
    //    cout << setprecision(8) << fixed;
}

template <typename T>
bool Point<T>::equals(Point<T> * point) {
    return equals_withinBound(point, 0.0000000000001);
}

template <typename T>
bool Point<T>::equals_withinBound(Point<T> * point, double error) {

    if (point == NULL) {
        return false;
    }
    if (this->size() != point->size()) {
        return false;
    }
    for (size_t i = 0; i<this->size(); i++) {
        double temp = fabs((*point)[i]-(*this)[i]);
        if (temp > error) {
            return false;
        }
    }

    return true;
}

template <typename T>
T Point<T>::getX() {
    return (*this)[0];
}

template <typename T>
T Point<T>::getY() {
    return (*this)[1];
}
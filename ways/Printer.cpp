#include "Printer.h"
#include "Configuration.h"
#include <iostream>
#include <algorithm>
#include <cfloat>
#include <fstream>

//#include <vector>
//#include <cstdlib>
//#include <cstring>

#define TAG "/mT" // change to: "mT" to print array of points for Mean Tracks in Output instead of just the "tag"


using namespace std;

bool compareString(const string left, const string right) {
    int x = atoi(left.c_str());
    int y = atoi(right.c_str());
    return x < y;

}

Printer::Printer() {
}

void Printer::print(Input * input) {
    cout << "****************************" << endl;
    cout << "\tInput info\t " << endl;
    cout << "****************************" << endl;
    cout << " Dimensions:\t" << input->d << endl;
    cout << " Ways:\t\t" << input->trackSet->size() << endl;
    if (input->func == DFT) {
        cout << " Metric:\tDFT" << endl;
    } else {
        cout << " Metric:\tDTW" << endl;
    }
    if (input->hash == CLASSIC) {
        cout << " Hash:\t\tCLASSIC" << endl;
    } else {
        cout << " Hash:\t\tPROBABILISTIC" << endl;
    }
    cout << "****************************" << endl << endl;

    //    if (input->tracks->size() < 100) {
    //        for (size_t i = 0; i < input->tracks->size(); i++) {
    //            (*(input->tracks))[i]->print();
    //        }
    //    } else {
    //        cout << "Dataset too big to be printed" << endl;
    //    }
}

void Printer::print(Output* output) {
    cout << "Algorithm: I" << output->init << "A" << output->assign << "U" << output->update << endl;
    cout << "Metric: ";
    if (output->func == DFT) {
        cout << "DFT" << endl;
    } else {
        cout << "DTW" << endl;
    }
    this->print(output->config);
    cout << "clustering_time: " << output->duration << endl;
    this->print(output->silhouette);
    if (true) {
        this->print(output->config->array);
    }
    cout << endl;
}

void Printer::print(Output* output, const char* outStream) {
    ofstream out(outStream);
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(out.rdbuf()); //redirect cout to <output file>
    cout << "k: " << output->config->array->size() << endl;
    if (output->init == 2){
        cout << "lsh_k: " << output->update << endl;
    }
    cout << "clustering_time: " << output->duration << endl;
    this->print(output->silhouette);
    if (true) {
        this->print(output->config->array);
    }
    cout << endl;
    cout.rdbuf(coutbuf);
}

void Printer::print(Array_<Output*>* outputs, const char* outStream, bool complete) {
    ofstream out(outStream);
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(out.rdbuf()); //redirect cout to <output file>
    size_t size_outputs = outputs->size();
    for (size_t i = 0; i < size_outputs; i++) {
        Output* output = ((*outputs))[i];
        if (output == NULL) {
            continue;
        }
        cout << "Algorithm: I" << output->init << "A" << output->assign << "U" << output->update << endl;
        cout << "Metric: ";
        if (output->func == DFT) {
            cout << "DFT" << endl;
        } else {
            cout << "DTW" << endl;
        }
        this->print(output->config);
        cout << "clustering_time: " << output->duration << endl;
        this->print(output->silhouette);
        if (complete) {
            this->print(output->config->array);
        }
        cout << endl;
    }
    cout.rdbuf(coutbuf);
}

void Printer::print(Configuration<TrackD*>* configuration) {
    size_t array_size = configuration->array->size();
    //    size_t nearest_size = configuration->nearestItems.size();

    for (size_t i = 0; i < array_size; i++) {
        size_t clusterItems = (*(configuration->array))[i]->items.size();
        string tag = (*(configuration->array))[i]->center->tag;
        //        size_t centroidSize = (*(configuration->array))[i]->center->size();
        cout << "CLUSTER-" << i << "\t{size: " << clusterItems << ", centroid: ";
        if (tag == TAG) {
            (*(configuration->array))[i]->center->print();
        } else {
            cout << tag << "}"; // << " (: " << centroidSize << ")";
        }
        cout << endl;
    }

}

void Printer::print(CentroidArray<TrackD*> * cArray) {
    size_t array_size = cArray->size();
    for (size_t i = 0; i < array_size; i++) {
        Centroid<TrackD*>*c = (*cArray)[i];
        size_t items_size = c->items.size();
        cout << "CLUSTER-" << i << "\t{";
        for (size_t j = 0; j < items_size - 1; j++) {
            cout << (*(c->items[j])).tag << ", ";
        }
        cout << (*(c->items[items_size - 1])).tag << "}" << endl;
    }
}

void Printer::print(Evaluation * evaluation) {
    cout << "silhouette:\t" << evaluation->evaluationValue << endl;
//    size_t eval_size = evaluation->silhouettePerCluster.size();
//    for (size_t i = 0; i < eval_size; i++) {
//        cout << evaluation->silhouettePerCluster[i] << ", ";
//    }
//    cout << evaluation->evaluationValue << "}" << endl;

}

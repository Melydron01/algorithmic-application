#ifndef PRINTER_H
#define PRINTER_H

#include "Input.h"
#include "Output.h"

class Printer {
public:
    Printer();
    void print(Input* input);
    void print(Output* output);
    void print(Output* output, const char* outStream);
    void print(Array_<Output*>* outputs, const char* outStream, bool complete);
    void print(Configuration< TrackD * >* configuration);
    void print(CentroidArray<TrackD*> * cArray);
    void print(Evaluation* evaluation);
private:

};

#endif /* PRINTER_H */


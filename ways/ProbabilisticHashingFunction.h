#ifndef PROBABILISTIC_HASHING_FUNCTION_H
#define PROBABILISTIC_HASHING_FUNCTION_H

#include "H_Euclidean.h"

template <typename T>
class ProbabilisticHashingFunction : public HashingFunction<T> {
public:

    H_Euclidean<T> ** h_euclidean;
    const int K_VEC;
    const int M;
    int * r;

    ProbabilisticHashingFunction(int v_dimension) : K_VEC(DEFAULT_KVEC), M(DEFAULT_M_FOR_VTW) {
        h_euclidean = new H_Euclidean<T> *[K_VEC];
        for (int i = 0; i < K_VEC; i++) {
            h_euclidean[i] = new H_Euclidean<T>(v_dimension);
        }
        r = new int[K_VEC];

        for (int i = 0; i < K_VEC; i++) {
            r[i] = (int) RandomGenerator::rangeIncl(0, RAND_MAX / 2);
        }
    }

    ~ProbabilisticHashingFunction() {
        for (int i = 0; i < K_VEC; i++) {
            delete h_euclidean[i];
        }
        delete [] h_euclidean;
        delete [] r;
    }

    virtual unsigned int hash(ProjectionVector<T> * mergedvector) {
        unsigned long long int temp[K_VEC];
        
        for (int i=0;i<K_VEC;i++) {
            temp[i] =  h_euclidean[i]->hash(mergedvector);
        }
        
        unsigned long long int res = 0;
        for (int i=0;i<K_VEC;i++) {
            res += ((unsigned long long int)(temp[i]*r[i])) % M;
        }
        
        res = res % M;        
        
        return (unsigned int) res;
    }
};



#endif /* PROBABILISTICHASHINGFUNCTION_H */


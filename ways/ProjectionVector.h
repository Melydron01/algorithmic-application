#ifndef PROJECTIONVECTOR_H
#define PROJECTIONVECTOR_H

#include "Array.h"
#include "Config.h"

template <typename T>
class ProjectionVector : public Array_<T> {
public:

    double value;

    ProjectionVector() {
    }

    ProjectionVector(int size) : Array_<T>(size) {
    }

    bool equals(ProjectionVector<T> * target, double error = DEFAULT_ERROR) {
        if (target == NULL) {
            return false;
        }
        if (this->size() != target->size()) {
            return false;
        }
        for (size_t i = 0; i<this->size(); i++) {
            double x = (*this)[i];
            double y = (*target)[i];
            double temp = fabs(x-y);
            if (temp > error) {
                return false;
            }
        }
        return true;
    }
};

#endif /* PROJECTIONVECTOR_H */


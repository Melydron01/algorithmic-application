#include "RandomGenerator.h"

#include <cstdlib>
#include <cmath>
#include <iostream>

using namespace std;

double RandomGenerator::doubleAll() { // [ -RAND_MAX/2, +RAND_MAX/2-1 ]
    int i = rand();
    double j = i - RAND_MAX * 0.5;
    if (j < 0)
        j = -j;
//    cout << " Got rand: " << j << endl;

    return j;
}

double RandomGenerator::normalDistribution() {
    double u;
    double v;
    double S;

    do {
        u = rangeExcl(0, 2);
        v = rangeExcl(0, 2);

        if (u==0 || v==0) {
            continue;            
        }
        u--;
        v--;
        S = u * u + v*v;
    } while (S == 0 || S > 1);

    double x = u * sqrt(-2 * log(S) / S);

    return x;
}


/*******************************************************************/
//double RandomGenerator::normalDistribution(int size , int offset=0) {
//    
//    return 1;
//}

double RandomGenerator::rangeIncl(double min, double max) {
    int cardinality = max - min;

    double r = min + (rand() / (RAND_MAX + 0.0))*(cardinality);
    return r;
}

double RandomGenerator::rangeExcl(double min, double max) {
    int cardinality = max - min + 1;

    double r = min + (rand() / (RAND_MAX + 1.0))*(cardinality);
    return r;
}


#ifndef RANDOMINITIALIZATION_H
#define RANDOMINITIALIZATION_H

#include <cstring>

#include "Initialization.h"
#include "RandomGenerator.h"

template <typename T>
class RandomInitialization : public Initialization<T> {
public:

    virtual ~RandomInitialization() {

    }

    virtual CentroidArray<T> * initialize(Array_<T> * set, Distance<T> * calculator, size_t k) {
        size_t N = set->size();

        if (k > N) {
            return NULL;
        }

        bool * selected = new bool[N];
        memset((void*) selected, 0, sizeof (bool) * N);

        CentroidArray<T> * array = new CentroidArray<T>();

        for (size_t i = 0; i < k; i++) {
            int index = 0;

            do {
                index = (int) RandomGenerator::rangeIncl(0, N - 1);
            } while (selected[index] == true);

//            Centroid<T> * centroid = new Centroid<T>((*set)[index]);
            
            Centroid<T> * centroid = new Centroid<T>((*set)[index], index);


            array->push_back(centroid);
            selected[index] = true;
        }

        //        

        delete [] selected;

        return array;
    }
private:

};

#endif /* RANDOMINITIALIZATION_H */


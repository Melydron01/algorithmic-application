/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HashTableElement.h
 * Author: melydron
 *
 * Created on October 25, 2017, 6:45 PM
 */

#ifndef SEGHASHTABLEELEMENT_H
#define SEGHASHTABLEELEMENT_H

#include "SegPoint.h"


template<typename T>
class SegHashTableElement {
public:
    SegPoint<T>* point;

    SegHashTableElement(SegPoint<T>* point) : point(point) {
    }
};


#endif /* HASHTABLEELEMENT_H */


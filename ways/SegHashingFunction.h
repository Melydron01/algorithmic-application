#ifndef SEGHASHINGFUNCTION_H
#define SEGHASHINGFUNCTION_H

#include <vector>
#include <sstream>

#include "Point.h"
#include "SegPoint.h"

using namespace std;

template <typename T>
class SegHashingFunction {
private:

    unsigned hash(const char *str) { // djb2 hash function by Dan Bernstein
        unsigned int hash = 5381;
        unsigned int c;
        while ((c = *str++))
            hash = ((hash << 5) + hash) + c; // hash * 33 + c
        return hash;
    }
public:

    virtual ~SegHashingFunction() {

    }

    virtual unsigned int hash(Point<T> * p) {
        T x = p->getX();
        T y = p->getY();
        
        // ---------------------
        // possible rounding ... for X,Y
        // ---------------------
        
        stringstream ss;

        // -------------------------------
        ss << x << "," << y;
        // -------------------------------

        string s = ss.str();
        const char * ptr = s.c_str();
        
        return hash(ptr);
    }
};

#endif /* ARRAY_H */


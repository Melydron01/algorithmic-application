/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SegHashtable.h
 * Author: melydron
 *
 * Created on October 10, 2017, 6:35 PM
 */

#ifndef SEGHASHTABLE_H
#define SEGHASHTABLE_H

#include "TrackDistance.h"
#include "List.h"
#include "SegHashingFunction.h"
#include "SegHashTableElement.h"
#include "SegPoint.h"
#include "PointDistance.h"

template<typename T>
class SegHashtable {
public:
    SegHashtable(int M, PointDistance<T> * pointDistanceCalc, SegHashingFunction<T> * hf);
    virtual ~SegHashtable();
    
    void insert(SegPoint<T> * point);
    
    SegPoint<T>* find(SegPoint<T> * point);
    SegPoint<T>* findByAddress(Point<T> * point);
    
    void printBuckets();
private:
    const int TABLE_SIZE;
    
    PointDistance<double> * distanceCalc;    
    SegHashingFunction<T> * hf;
    List<SegHashTableElement<T>*> ** lists;
};

#include "SegHashtable.hpp"

#endif /* HASHTABLE_H */


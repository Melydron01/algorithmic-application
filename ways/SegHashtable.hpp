#include <float.h>
#include <vector>

#include "List.h"
#include "SegHashingFunction.h"
#include "SegHashTableElement.h"
#include "Track.h"
#include "SegPoint.h"

template<typename T>
SegHashtable<T>::SegHashtable(int M, PointDistance<T> * pointDistanceCalc, SegHashingFunction<T> * hf) : TABLE_SIZE(M), distanceCalc(pointDistanceCalc), hf(hf) {
    lists = new List<SegHashTableElement<T>*>*[M];
    for (int i = 0; i < M; i++) {
        lists[i] = new List<SegHashTableElement<T>*>();
    }
}

template<typename T>
SegHashtable<T>::~SegHashtable() {
    for (int i = 0; i < TABLE_SIZE; i++) {
        lists[i]->removeAll();
        delete lists[i];
    }

    delete [] lists;

    lists = NULL;
}

template<typename T>
void SegHashtable<T>::insert(SegPoint<T> * segpoint) {
    unsigned int offset = hf->hash(segpoint->point) % TABLE_SIZE;

    SegHashTableElement<T> * element = new SegHashTableElement<T>(segpoint);

    lists[offset]->insertFront(element);
}

template<typename T>
SegPoint<T>* SegHashtable<T>::find(SegPoint<T> * segpoint) {
    unsigned int offset = hf->hash(segpoint->point) % TABLE_SIZE;
    SegPoint<T> * find = NULL;

    for (ListNode< SegHashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        SegHashTableElement<T> * he = p->getData();
        SegPoint<T> * t2 = he->point;

        double tempDistance = distanceCalc->simplified(segpoint->point, t2->point);
        if (tempDistance < DEFAULT_ERROR) {
            find = t2;
        }
    }

    return find;
}

template<typename T>
SegPoint<T>* SegHashtable<T>::findByAddress(Point<T> * point) {
    unsigned int offset = hf->hash(point) % TABLE_SIZE;
    SegPoint<T> * find = NULL;

    for (ListNode< SegHashTableElement<T>* > * p = lists[offset]->getHead(); p != NULL; p = p->getNext()) {
        SegHashTableElement<T> * he = p->getData();
        SegPoint<T> * t2 = he->point;

        if (t2->point == point) {
            find = t2;
            break;
        }
    }
    return find;
}

template<typename T>
void SegHashtable<T>::printBuckets() {
//    cout << "Total Buckets: " << TABLE_SIZE << endl;
    for (int i = 0; i < TABLE_SIZE; i++) {
        int list_size = lists[i]->getSize();
        if (list_size == 0) {
            continue;
        }
        cout << "#" << i << " items: " << list_size << endl;

        ListNode < SegHashTableElement <T> *> * temp = lists[i]->getHead();

        SegHashTableElement<T> * element;
        for (int j = 0; j < list_size; j++) {
            element = temp->getData();
            cout << element->point->index<< " " << 
                    element->point->point->getX() << " " << 
                    element->point->point->getY() << " " <<
                    element->point->parent->tag << endl;
            
            temp = temp->getNext();
        }
        cout << endl;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SegPoint.h
 * Author: melydron
 *
 * Created on January 9, 2018, 9:20 AM
 */

#ifndef SEGPOINT_H
#define SEGPOINT_H

#include "Point.h"
#include "Track.h"

template <typename T>
class SegPoint {
public:
    Track<T> * parent;
    Point<T> * point;
    unsigned int index;

    bool isEdge() {
        if (parent->getFirst() == point || parent->getLast() == point ) {
            return true;
        } else {
            return false;
        }
    }
    
    SegPoint(Track<T>* parent, Point<T>* point, unsigned int index) : parent(parent), point(point), index(index) {
    }
};

#endif /* SEGPOINT_H */


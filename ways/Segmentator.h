#ifndef SEGMENTATOR_H
#define SEGMENTATOR_H

#include "SegHashtable.h"
#include "EuclideanDistance.h"

#define RADIUS_THRESHOLD (0.5)

class Segmentator {
public:
    SegHashtable<double> hashtable;

    Segmentator(PointDistance<double> * pointDistanceCalc, SegHashingFunction<double> * hf) : hashtable(19213, pointDistanceCalc, hf) {

    }

    virtual void print() {
        hashtable.printBuckets();
    }

    bool angleSeg(PointD* p1, PointD* p2, PointD* p3) {
        PointD * pointA = p1;
        PointD * pointB = p2;
        PointD * pointC = p3;
        EuclideanDistance<double> calculator;

        double a = calculator.calculate(pointB, pointC);
        double b = calculator.calculate(pointA, pointC);
        double c = calculator.calculate(pointA, pointB);

        double xA = (*pointA)[0];
        double yA = (*pointA)[1];

        double xB = (*pointB)[0];
        double yB = (*pointB)[1];

        double xC = (*pointC)[0];
        double yC = (*pointC)[1];

        //        cout << "(" << xA << "," << yA << ")" << endl;
        //        cout << "**********************" << endl;
        //        cout << "(" << xB << "," << yB << ")" << endl;
        //        cout << "**********************" << endl;
        //        cout << "(" << xC << "," << yC << ")" << endl;
        //        cout << "**********************" << endl;

        if ((xA - xB) == 0) {
            if ((xB - xC) == 0) { // same line
                return false;
            }
        } else {
            double lab = (yA - yB) / (xA - xB);
            double lbc = (yB - yC) / (xB - xC);


            if (lab - lbc == 0) { // same line
                return false;
            } else {
                double radius = (a + b + c)*(-a + b + c)*(a - b + c)*(a + b - c);
                radius = sqrt(radius);
                radius = (a * b * c) / radius;
                if (radius > RADIUS_THRESHOLD) {
                    return true;
                }
            }
        }
        return false;
    }

    virtual void insert(TrackSet * trackSet, Track<double>* track) {
        //        cout << "Inserting: " << track->tag << endl;
        for (unsigned int i = 0; i < track->getLength(); i++) {
            Point<double> * p = (*track)[i];
            SegPoint<double> * newpoint = new SegPoint<double>(track, p, i);

            SegPoint<double> * oldpoint = hashtable.find(newpoint);


            if (oldpoint != NULL && oldpoint->index > 3 && oldpoint->index < oldpoint->parent->getLength() - 3) {
                // old track edge - new track edge
                if (newpoint->isEdge() && oldpoint->isEdge()) {
                    hashtable.insert(newpoint);
                    continue;
                }

                // old track edge - new track inner
                if (oldpoint->isEdge() && !newpoint->isEdge()) { // split new
                    //                                        cout << " a " << endl;
                    hashtable.insert(newpoint);

                    Track<double>* subtrack = track->split(i);
                    trackSet->push_back(track);
                    insert(trackSet, subtrack);
                    //                    cout << " b " << endl;

                    return;
                }

                // old track inner - new track edge
                if (!oldpoint->isEdge() && newpoint->isEdge()) { // split new
                    //                    cout << " a " << endl;

                    hashtable.insert(newpoint);

                    Track<double>* subtrack = oldpoint->parent->split(oldpoint->index);
                    trackSet->push_back(subtrack);

                    Point<double> * copy = new Point<double>(subtrack->getFirst()->getX(), subtrack->getFirst()->getY());
                    SegPoint<double> * newpoint = new SegPoint<double>(subtrack, copy, 0);
                    hashtable.insert(newpoint);

                    for (unsigned int j = 1; j < subtrack->getLength(); j++) {
                        Point<double> * q = (*subtrack)[j];
                        SegPoint<double> * segpoint = hashtable.findByAddress(q);
                        segpoint->index = j;

                        //                        cout << " change: from " << segpoint->parent->tag << " to " << subtrack->tag << endl;

                        segpoint->parent = subtrack;

                    }
                    //                    cout << " b " << endl;

                }

                // old track inner - new track inner
                if (!oldpoint->isEdge() && !newpoint->isEdge()) { // split new
                    hashtable.insert(newpoint);

                    Track<double>* subtrack = oldpoint->parent->split(oldpoint->index);
                    trackSet->push_back(subtrack);

                    Point<double> * copy = new Point<double>(subtrack->getFirst()->getX(), subtrack->getFirst()->getY());
                    SegPoint<double> * newpoint = new SegPoint<double>(subtrack, copy, 0);
                    hashtable.insert(newpoint);
                    for (unsigned int j = 1; j < subtrack->getLength(); j++) {
                        Point<double> * q = (*subtrack)[j];
                        SegPoint<double> * segpoint = hashtable.findByAddress(q);
                        segpoint->index = j;
                        segpoint->parent = subtrack;
                    }
                    // ----------------------------------------

                    Track<double>* subtrack2 = track->split(i);
                    trackSet->push_back(track);
                    insert(trackSet, subtrack2);
                    return;
                }
            } else {
                hashtable.insert(newpoint);
            }

        }

        trackSet->push_back(track);
    }
};


#endif /* SEGMENTATOR_H */


#ifndef SILHOUETTE_H
#define SILHOUETTE_H

#include <cfloat>

#include "Configuration.h"
#include "Input.h"
#include "Evaluation.h"

template <typename T>
class Silhouette {
public:

    Evaluation * calculate(Configuration<TrackD*> * configuration, DistanceMatrix<T> * matrix) {
        size_t K = configuration->array->size();

        double total_si = 0;
        double avg_si = 0;

        Evaluation * evaluation = new Evaluation();

        // for each cluster ...
        for (size_t i = 0; i < K; i++) { // for each centroid
            Centroid<T> * center = (*(configuration->array))[i];
            size_t items_size = center->items.size();

            double total_cluster_si = 0;
            double avg_cluster_si = 0;

            for (size_t j = 0; j < items_size; j++) { // for each non centroid in the specific cluster
                double si = 0;
                double ai = 0;
                double bi = 0;
                //                T t = center->items[j]; // pick the candidate for the swap
                int t = center->item_infos[j]->index; // pick the candidate for the swap

                // calculate inner cost
                //                cout << "Calculating inner cost for item: " << j << endl;
                for (size_t inner_i = 0; inner_i < items_size; inner_i++) {
                    if (inner_i != j) {
                        //                        T item_in_same_cluster = center->items[inner_i];
                        int item_in_same_cluster = center->item_infos[inner_i]->index;
                        //                        T second_best_centroid = m->items[inner_i]
                        //                        double d = calculator->simplified(item_in_same_cluster, t);
                        double d = matrix->getDistance(t, item_in_same_cluster);
                        ai = ai + d;
                    }
                }
                //                cout << "Done." << endl;

                Centroid<T> * secondBestCentroid = center->item_infos[j]->secondBestCentroid;

                if (secondBestCentroid == NULL) {
                    // calculate ...
                    //                    T & item = t;

                    // find 
                    double secondbestDistance = DBL_MAX;

                    for (size_t outer_c = 0; outer_c < K; outer_c++) { // for each other cluster
                        if (outer_c != i) {
                            Centroid<T> * other_cluster = (*(configuration->array))[outer_c];
                            int center_index = other_cluster->index;

                            //                            double d = calculator->simplified(item, other_cluster->center);
                            double d = matrix->getDistance(t, center_index);

                            if (d < secondbestDistance) {
                                secondbestDistance = d;
                                secondBestCentroid = other_cluster;
                            }
                        }
                    }
                }

                if (secondBestCentroid != NULL) {
                    for (size_t outer_i = 0; outer_i < secondBestCentroid->items.size(); outer_i++) {
                        //                        T item_in_other_cluster = secondBestCentroid->items[outer_i];
                        int item_in_other_cluster = secondBestCentroid->item_infos[outer_i]->index;

                        //                        double d = calculator->simplified(item_in_other_cluster, t);
                        double d = matrix->getDistance(item_in_other_cluster, t);
                        bi = bi + d;
                    }
                } else {
                    cout << "?? -- Evaluation (Second best centroid not found) " << endl;
                    bi = 1;
                }

                ai = ai / (items_size - 1);
                bi = bi / (secondBestCentroid->items.size());

                if (max(ai, bi) != 0) {
                    si = (bi - ai) / max(ai, bi);
                } else {
                    si = 1;
                }
                total_cluster_si += si;
            }

            if (items_size != 0) {
                avg_cluster_si = total_cluster_si / items_size;
            } else {
                avg_cluster_si = 1;
            }
            evaluation->silhouettePerCluster.push_back(avg_cluster_si);

            total_si = total_si + avg_cluster_si;
        }

        if (K != 0) {
            avg_si = total_si / K;
        } else {
            avg_si = 1;
        }

        evaluation->evaluationValue = avg_si;

        return evaluation;
    }

      Evaluation * calculate(Configuration<TrackD*> * configuration, Distance<TrackD*> * calculator) {
        size_t K = configuration->array->size();

        double total_si = 0;
        double avg_si = 0;

        Evaluation * evaluation = new Evaluation();

        // for each cluster ...
        for (size_t i = 0; i < K; i++) { // for each centroid
            Centroid<T> * center = (*(configuration->array))[i];
            size_t items_size = center->items.size();

            double total_cluster_si = 0;
            double avg_cluster_si = 0;

            for (size_t j = 0; j < items_size; j++) { // for each non centroid in the specific centroid
                double si = 0;
                double ai = 0;
                double bi = 0;
                T t = center->items[j]; // pick the candidate for the swap

                // calculate inner cost
                //                cout << "Calculating inner cost for item: " << j << endl;
                for (size_t inner_i = 0; inner_i < items_size; inner_i++) {
                    if (inner_i != j) {
                        T item_in_same_cluster = center->items[inner_i];
                        //                        T second_best_centroid = m->items[inner_i]
                        double d = calculator->simplified(item_in_same_cluster, t);
                        ai = ai + d;
                    }
                }
                //                cout << "Done." << endl;

                Centroid<T> * secondBestCentroid = center->item_infos[j]->secondBestCentroid;

                if (secondBestCentroid == NULL) {
                    // calculate ...
                    T & item = t;

                    // find 
                    double secondbestDistance = DBL_MAX;

                    for (size_t outer_c = 0; outer_c < K; outer_c++) { // for each other cluster
                        if (outer_c != i) {
                            Centroid<T> * other_cluster = (*(configuration->array))[outer_c];

                            double d = calculator->simplified(item, other_cluster->center);

                            if (d < secondbestDistance) {
                                secondbestDistance = d;
                                secondBestCentroid = other_cluster;
                            }
                        }
                    }
                }

                if (secondBestCentroid != NULL) {                    
                    for (size_t outer_i = 0; outer_i < secondBestCentroid->items.size(); outer_i++) {
                        T item_in_other_cluster = secondBestCentroid->items[outer_i];

                        double d = calculator->simplified(item_in_other_cluster, t);
                        bi = bi + d;
                    }
                } else {
                    cout << "???????????????????? secondBestCentroid not found  " << endl;
                    bi = 1;
                }

                ai = ai / (items_size - 1);
                bi = bi / (secondBestCentroid->items.size());

                if (max(ai, bi) != 0) {
                    si = (bi - ai) / max(ai, bi);
                } else {
                    si = 1;
                }
                total_cluster_si += si;
            }

            if (items_size != 0) {
                avg_cluster_si = total_cluster_si / items_size;
            } else {
                avg_cluster_si = 1;
            }
            evaluation->silhouettePerCluster.push_back(avg_cluster_si);

            total_si = total_si + avg_cluster_si;
        }

        if (K != 0) {
            avg_si = total_si / K;
        } else {
            avg_si = 1;
        }

        evaluation->evaluationValue = avg_si;

        return evaluation;
    }


};


#endif /* SILHOUETTE_H */


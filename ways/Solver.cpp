#include <iostream>
#include <valarray>

#include "Solver.h"

using namespace std;

Solver::Solver(Input* input) {
    this->input = input;
    //    PointDistance<double> * pointDistanceCalc = new EuclideanDistance<double>();
}

Solver::~Solver() {

}

Configuration<TrackD*> * Solver::run(bool verbose, Initialization< TrackD * > * init, Assignment< TrackD * > * assignement, Update< TrackD* > * update) {
    size_t k = input->clusters_K;
    long int counter = 0;

    if (verbose)
        cout << " Initialization..." << endl;
    CentroidArray<TrackD *> * centroids = init->initialize(input->trackSet, input->matrix, k);

    if (verbose)
        cout << " Assignment..." << endl;
    Configuration< TrackD * > * config = assignement->assign(centroids, input->trackSet, input->matrix);

    if (verbose)
        printer.print(config);

    while (counter < 15) {
        if (verbose)
            cout << " Update ... #" << counter << endl;
        centroids = update->update(config, input->matrix);

        if (centroids == NULL) {
            if (verbose)
                cout << "-- No further update possible!" << endl;
            break;
        }
        if (verbose)
            cout << " Assignment... #" << counter << endl;

        config = assignement->assign(centroids, input->trackSet, input->matrix);

        if (verbose)
            printer.print(config);

        counter++;
    }

    return config;
}

Output* Solver::solve(bool verbose) { // Kmeans -> Lloyd -> PAM
    Output* output = new Output();
    Distance<TrackD * > * trackDistCalculator;
    output->init = 1;
    output->update = 0;
    if (input->func == DFT) {
        output->func = DFT;
        trackDistCalculator = new FrechetDistance<double>();
    } else {
        output->func = DTW;
        trackDistCalculator = new TimeWarpDistance<double>();
    }
    Initialization< TrackD * > * init = new KMeansInitialization<TrackD *>();
    Assignment< TrackD * > * assignement = new LloydAssignment<TrackD *>();
    Update< TrackD* > * update = new PamUpdate<TrackD *>();
    double maxValue = -2;
    //    int k_given = input->clusters_K;
    int inputSize = input->trackSet->size();
    cout << "Will check for optimal k in range: [2, " << inputSize / 2 << "]" << endl << endl;
    int noImprove = 0;
    for (int i = 2; i < inputSize / 2; i++) {
        input->clusters_K = i;
        clock_t begin = clock();
        Configuration<TrackD*> * config = run(verbose, init, assignement, update);
        clock_t end = clock();
        Silhouette<TrackD*> silhouette;
        Evaluation* eval = silhouette.calculate(config, input->matrix);
        double value = eval->evaluationValue;
        if (value > maxValue) {
            noImprove = 0;
            maxValue = value;
            cout << "Current best Silhouette: " << value << "\t[k = " << i << "]" << endl;
            double duration = ((double) (end - begin) / CLOCKS_PER_SEC);
            output->silhouette = eval;
            output->duration = duration;
            output->config = config;
            if (value > 0.75) {
                char c;
                cout << "Found silhouette above 0.75. Stop checking? (y/n):";
                cin >> c;
                if (toupper(c) == 'Y') break;
            }
        } else {
            noImprove++;
            //            if (noImprove > 30) {
            //                noImprove = 0;
            //                char c;
            //                cout << "No improvement last 30 iterations [current k: " << i << "]. Stop checking? (y/n):";
            //                cin >> c;
            //                if (toupper(c) == 'Y') break;
            //            }
            delete config;
            delete eval;

        }

    }
    delete init;
    delete assignement;
    delete update;
    delete trackDistCalculator;
    return output;
}

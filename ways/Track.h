#ifndef TRACK_H
#define TRACK_H

#include <string>
#include <vector>

#include "List.h"
#include "Point.h"
#include "Distance.h"

using namespace std;

template<typename T>
class Track : public Array_< Point<T>* > {
public:
    Track();
    Track(T * data, int m, int d);
    Track(string tag, int m);
    virtual ~Track();
    
    void addPoint(Point<T> * point, Distance< Point<T>* > * distance = 0);
    size_t getLength();
    
    void moveToCenter();
    T * getRowMajorMatrix();
    T * getColumnMajorMatrix();
    
    Point<T>* getFirst();
    Point<T>* getLast();
    
    void print();
    
    Track<T> * split(unsigned int index);
    
    
    const string tag;  
    bool equals(Track<T> * t2);        
};

typedef Track<double> TrackD;
typedef Array_<TrackD *> TrackSet;
//typedef Distance<Track<double> * > TrackDDistance;

#include "Track.hpp"

#endif /* TRACK_H */


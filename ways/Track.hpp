    #include "Track.h"
#include "Distance.h"

template<typename T>
Track<T>::Track() : tag("<no id>") {
    //    this->resize(m);
}

template<typename T>
Track<T>::Track(string tag, int m) : tag(tag) {
//    this->resize(m);
}

template<typename T>
Track<T>::~Track() {
    for (size_t i = 0; i<this->size(); i++) {
        delete (*this)[i];
    }
}

template<typename T>
size_t Track<T>::getLength() {
    return this->size();
}

template<typename T>
void Track<T>::addPoint(Point<T> * point, Distance< Point<T>* > * distCalculator) {
    if (this->size() > 0) {
        Point<T> * previousPoint = (*this)[this->size() - 1];

        if (point->equals_withinBound(previousPoint) == true) {
            delete point;
            return;
        }
    }
    this->push_back(point);
}

template<typename T>
void Track<T>::print() {
    size_t size = this->size();
    cout << tag << ", " << size << ", ";
    for (size_t i = 0; i<size; i++) {
        if (i > 0)
            cout << ", ";
        (*this)[i]->print();
    }
//    cout << "]";
}

template<typename T>
bool Track<T>::equals(Track<T> * track) {
    if (track == NULL) {
        return false;
    }
    if (this->size() != track->size()) {
        return false;
    }
    for (size_t i = 0; i<this->size(); i++) {
        if (!(*this)[i]->equals((*track)[i])) {
            return false;
        }
    }
    return true;
}

template<typename T>
void Track<T>::moveToCenter() {
    if (this->size() == 0) {
        return;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    for (size_t d = 0; d < D; d++) {
        T xMean = 0;
        for (size_t i = 0; i < N; i++) {
            xMean = xMean + (*((*this)[i]))[d];
        }
        xMean /= N;

        for (size_t i = 0; i < N; i++) {
            (*((*this)[i]))[d] -= xMean;
        }
    }
}

template<typename T>
T * Track<T>::getRowMajorMatrix() {
    if (this->size() == 0) {
        return NULL;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    T * data = new T[N * D];

    size_t j = 0;

    for (size_t i = 0; i < N; i++) {
        for (size_t d = 0; d < D; d++) {
            data[j++] = (*((*this)[i]))[d];
        }
    }

    return data;
}

template<typename T>
T * Track<T>::getColumnMajorMatrix() {
    if (this->size() == 0) {
        return NULL;
    }

    size_t D = (*this)[0]->getD();
    size_t N = this->size();

    T * data = new T[N * D];

    size_t j = 0;

    for (size_t d = 0; d < D; d++) {
        for (size_t i = 0; i < N; i++) {
            data[j++] = (*((*this)[i]))[d];
        }
    }

    return data;
}

template<typename T>
Track<T>::Track(T * data, int m, int d) : tag("mT") {
    int k = 0;
    for (int i = 0; i < m; i++) {
        Point<T> * point = new Point<T>(d);
        for (int j = 0; j < d; j++) {
            (*point)[j] = data[k++];
        }
        addPoint(point);
    }
}

template<typename T>
Point<T>* Track<T>::getFirst() {
    if (getLength() == 0) {
        return NULL;
    } else {
        return (*this)[0];
    }
}

template<typename T>
Point<T>* Track<T>::getLast() {
    if (getLength() == 0) {
        return NULL;
    } else {
        return (*this)[this->size() - 1];
    }
}

template<typename T>
Track<T> * Track<T>::split(unsigned int index) {
    char temp[100];
    sprintf(temp, "%d" , rand()%1000);
    string s = tag + "_" + temp;
    
    Track<T> * part = new Track<T>(s,0);
    for (unsigned int i = index; i<this->size(); i++) {
        if (i == index) {
            Point<T> * p = new Point<T>((*this)[i]->getX(), (*this)[i]->getY());
            part->addPoint(p);
        } else {
            part->addPoint((*this)[i]);
        }
            
    }
    this->resize(index + 1);
    return part;
}
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <time.h>
#include <valarray>
#include <iomanip>
#include "Loader.h"
#include "Input.h"
#include "Solver.h"
#include "ExecInfo.h"
#include "CRMSDDistance.h"
#include "LIFDistance.h"
#include "LITDistance.h"
#include "Segmentator.h"
#include "LSH_Solver.h"

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SVD>

using namespace std;

using Eigen::MatrixXd;

void segmentWays(const char* ways) {
    Loader * loader = new Loader();
    TrackSet* trackSet = loader->loadTracks(ways);
    const char* fileName = "../datasets/segment.csv";
    ofstream out(fileName);
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(out.rdbuf()); //redirect cout to <output file>
    for (size_t k = 0; k < trackSet->size(); k++) {
        //        cout << "--- Original ---" <<endl;
        cout << k << ", ";
        (*trackSet)[k]->print();
        cout << endl;
        //        (*trackSet)[k]->moveToCenter();
        //        cout << "---  Moved  ---" <<endl;
        //        (*trackSet)[k]->print();
        //        cout << endl;
    }
    cout.rdbuf(coutbuf);
    return;
}

int main(int argc, char** argv) {


    srand(time(0));
    cout << setprecision(7) << fixed;

    ExecInfo execInfo;
    if (!execInfo.argHandling(argc, argv)) {
        cerr << "Wrong arguments!!" << endl;
        execInfo.printHelp(argv[0]);
        return 0;
    }

    if (execInfo.segment){
      segmentWays(execInfo.inputFile);
      return 0;
    }

    // string temp;
    // if (!execInfo.inputFileFlag) {
    //     cout << "Give input file path: ";
    //     getline(cin, temp);
    //     execInfo.inputFile = temp.c_str();
    // }

    Loader * loader = new Loader();
    Input * input = loader->load(execInfo.inputFile);

    Distance<TrackD * > * trackDistCalculator;
    const char* distFunc;
    const char* outputFile;
    if (execInfo.metric == DFT) {
        distFunc = "DFT";
        outputFile = "./Outputs/kmeans_ways_frechet.dat";
        trackDistCalculator = new LIFDistance<double>();
        input->func = DFT;
    } else {
        distFunc = "DTW";
        outputFile = "./Outputs/kmeans_ways_dtw.dat";
        input->func = DTW;
        trackDistCalculator = new LITDistance<double>();
    }

    cout << "Input:\t" << execInfo.inputFile << endl;
    cout << "Output:\t" << outputFile << endl;
    string dataMatrixString;
    dataMatrixString = string(execInfo.inputFile) + "_" + distFunc + "_matrix.txt";
    const char* matrixFile = dataMatrixString.c_str();
    //    dataMatrix = execInfo.inputFile;
    //    cout << "dataMatrix file: " << dataMatrix << endl;


    input->matrix = new DistanceMatrix<TrackD *>(input->trackSet->size());

    if (execInfo.fileExists(matrixFile)) {
        cout << "Matrix file found. Will read pre-calculated distances." << endl;
        input->matrix->readFill(input->trackSet, trackDistCalculator, matrixFile);
    } else {
        cout << "Matrix file not found. Will calculate and write all distances." << endl;
        input->matrix->calcFill(input->trackSet, trackDistCalculator, matrixFile);
    }
    input->hash = CLASSIC;
    input->complete = false;
    Printer printer;
    printer.print(input);
    //    //
    cout << "K-Medoids Clustering..." << endl;
    Solver *solver = new Solver(input);
    //    //
    Output* output = solver->solve(false);
    cout << endl;
    cout << "Done. Printing result to: " << outputFile << endl;
    printer.print(output, outputFile);
    cout << "***********************" << endl << endl;
    cout << "LSH Clustering..." << endl;
    LSH_Solver* lsh_solver = new LSH_Solver();
    const char* outputFile2 = "./Outputs/lsh_ways_clustering.dat";
    Output* output2 = lsh_solver->solve(input);
    cout << endl;
    cout << "Done. Printing result to: " << outputFile2 << endl;
    printer.print(output2, outputFile2);
    cout << "***********************" << endl << endl;
    delete loader;
    delete solver;
    delete input;
    delete output;
    return 0;
}
